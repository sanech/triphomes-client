/// <reference types="vite/client" />
interface ImportMetaEnv {
    readonly VITE_APP_TITLE: string
    readonly VITE_APP_FB_API_KEY: string
    readonly VITE_APP_FB_AUTH_DOMAIN: string
    readonly VITE_APP_FB_DATABASE_URL: string
    readonly VITE_APP_FB_PROJECT_ID: string
    readonly VITE_APP_FB_STORAGE_BUCKET: string
    readonly VITE_APP_FB_MSG_ID: string
    readonly VITE_APP_FB_APP_ID: string
    readonly VITE_APP_FB_MID: string
    readonly VITE_APP_TRIPADVISOR: string
    readonly VITE_APP_AUTH_URL: string
    readonly VITE_APP_NP_KEY: string
    readonly VITE_APP_NP_TEST_KEY: string
    readonly VITE_APP_STADIAMAPS: string
    readonly VITE_APP_PROXY_URL: string
    // more env variables...
  }
  
  interface ImportMeta {
    readonly env: ImportMetaEnv
  } 