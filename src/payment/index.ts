import axios from 'axios';

const testing: boolean = true;
let nowPaymentsApiKey: string = '';


if(testing) {
    console.log('testing = ' + testing);
    nowPaymentsApiKey = import.meta.env.VITE_APP_NP_TEST_KEY;
    axios.defaults.baseURL = 'https://api-sandbox.nowpayments.io/v1';
} else {
    console.log('testing = ' + testing);
    nowPaymentsApiKey = import.meta.env.VITE_APP_NP_KEY;
    axios.defaults.baseURL = 'https://api.nowpayments.io/v1';
}

axios.defaults.headers.common['x-api-key'] = nowPaymentsApiKey;
axios.defaults.headers.post['Content-Type'] = 'application/json';

const Payment = {
    start: async () => {
        return new Promise((resolve, reject) => {
            axios.get('/status')
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error);
                });
        })
    },
    currencies: async () => {
        return new Promise((resolve, reject) => {
            axios.get('/currencies')
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error);
                });
        })
    },
    estimate: async (amount: number, currency_from: string, currency_to: string) => {
        return new Promise((resolve, reject) => {
            axios.get('/estimate' + '?amount=' + amount + '&currency_from=' + currency_from + '&currency_to=' + currency_to)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error);
                });
        })
    },
    create: async (obb: any) => {
        return new Promise((resolve, reject) => {
            axios.post('/payment', obb)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error);
                });
        })
    },
    status: async (payment_id: any) => {
        return new Promise((resolve, reject) => {
            axios.get('/payment/' + payment_id)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error);
                });
        })
    },
    minimum: async (currency_from: string, currency_to: string) => {
        return new Promise((resolve, reject) => {
            axios.get('/min-amount/' + '?currency_from=' + currency_from + '&currency_to=' + currency_to)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error);
                });
        })
    },
    invoice: async (obb: any) => {
        return new Promise((resolve, reject) => {
            axios.post('/invoice', obb)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error);
                });
        })
    }
}

export default Payment;