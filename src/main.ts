import { createApp, h } from 'vue';
import { createPinia } from 'pinia'
import { registerScrollSpy } from 'vue3-scroll-spy'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import App from './App.vue'
import router from './router'
import 'bootstrap'
import './assets/scss/style.scss'
// @ts-ignore: Could not find a declaration file for module 'vue-snap'.
import VueSnap from 'vue-snap'
import LoadScript from "vue-plugin-load-script";
import 'vue-snap/dist/vue-snap.css'
import { Spin } from 'ant-design-vue'
import i18n from './i18n';
import 'dayjs/locale/de';
import VueDatePicker from '@vuepic/vue-datepicker';
import '@vuepic/vue-datepicker/dist/main.css'

Spin.setDefaultIndicator({
    indicator: h('i', { class: 'custom-spinner' }),
});
const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);

const app = createApp(App);
app.use(LoadScript);
app.use(pinia);
app.use(router);
app.use(VueSnap);
app.use(i18n);
app.component('VueDatePicker', VueDatePicker);
registerScrollSpy(app);

router.isReady().then(() => {
    app.mount('#app');
}).finally(() => {
    const doc: HTMLElement|null = document.getElementById('loadingSpinner');
    if(doc) {
        doc.remove();
    }
});
