import PropertyView from '@/views/PropertyView.vue'
import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: '/p/:homeId',
      name: 'property',
      component: PropertyView,
      props: true,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: '/p/VacationRentalReview/:homeId',
      name: 'property',
      component: PropertyView,
      props: true,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: '/rooms/:homeId',
      name: 'rooms',
      component: PropertyView,
      props: true,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: '/BookingRequest',
      name: 'Booking',
      component: () => import('../views/BookingRequestView.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/BookingRequestStatus/:bookingId',
      name: 'BookingStatus',
      component: () => import('../views/BookingRequestStatusView.vue'),
      props: true,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: '/InvoiceView/:bookingId',
      name: 'InvoiceView',
      component: () => import('../views/InvoiceView.vue'),
      props: true,
      meta: {
        requiresAuth: false
      }
    },
  ]
});
export default router
