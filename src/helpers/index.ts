import { reactive } from 'vue';
import { gc, ciso } from './countries'
const env = import.meta.env.PROD;

export const visit = (url: string) => {
  window.location.href = url;
}
export const download = (url: string) => {
  const link = document.createElement('a');
  link.href = url;
  link.setAttribute('download', '');
  document.body.appendChild(link);
  link.click();
}
export const getContinent = gc;
export const countries = ciso;
interface Home {
    title: string;
    prices: object;
    location: object;
    images: Array<{
        title: string;
        src: string;
    }>;
}

export interface Booking {
  paid: boolean;
  plisio: any | null;
  code: string | null,
  created: Date | null,
  edited: string | null,
  fullpayment: boolean | null,
  work: boolean,
  message: string | null,
  purpose: string | null,
  other: string | null,
  guest: {
    given_name: string | null,
    family_name: string | null,
    email: string | null,
    phone: string | null,
    address: string | null,
    city: string | null,
    state: string | null,
    country: {
      name: string,
      code: string,
    },
    zipCode: string | null,
  },
  userdata: any | null,
  userAgent: any | null,
  home: {
    name: string,
    id: string | number,
    image: string,
  },
  stay: {
    checkin: {
      parsed: string,
    },
    checkout: {
      parsed: string,
    },
    nights: number,
    guests: number,
    months: number,
  },
  gateway: {
    details: string | null,
    gateway: string | null,
    icon: string | null,
    id: string | null,
    instructions: string | null,
    name: string | null,
    uid: string | null
  },
  dates: string[],
  payment: {
      part: boolean,
      dueNow: number,
      dueNowText: string,
      dueNext: number,
      dueNextText: string,
  },
  coupon: {
      id: string | null,
      code: string | null,
      validity: string[] | null,
      discount: number,
      message: string | null,
      active: boolean,
      uid: string | null,
  },
  status: {
      name: string | null,
      value: string | null,
      info: string | null,
  },
  uid: any,
}

function roundUp(num: number, precision: number) {
  precision = Math.pow(10, precision)
  return Math.ceil(num * precision) / precision
}

export const redirect: any = (str: string | number = 'notFound') => {
  if(env === true) {
    window.location.href = "https://www.tripadvisor.com/" + str;
  } else {
    console.log('Development mode ...', "https://www.tripadvisor.com/" + str);
  }
}
export const homeRating = (reviews: string[]) => {
  var total = 0;
  // get total rating from reviews.rating
  reviews.forEach((arr: any) => total += arr.rating);

  total = (total / reviews.length);
  total = roundUp(total, 1);
  return total;
}

export const home: Home = {
    title: '105-Suite ELIOT, Lovely Duplex, Great Location',
    prices: {
        nightly: 100,
        weekly: 500,
        monthly: 1500,
    },
    images: [
      {
        title: '',
        src: 'https://media-cdn.tripadvisor.com/media/vr-splice-j/0a/ca/f9/cb.jpg',
      },
      {
        title: '',
        src: 'https://media-cdn.tripadvisor.com/media/vr-splice-j/0a/ca/f9/c6.jpg',
      },
      {
        title: '',
        src: 'https://media-cdn.tripadvisor.com/media/vr-splice-j/0a/ca/f9/c5.jpg',
      },
      {
        title: '',
        src: 'https://media-cdn.tripadvisor.com/media/vr-splice-j/0a/ca/f9/c4.jpg',
      },
      {
        title: '',
        src: 'https://media-cdn.tripadvisor.com/media/vr-splice-j/0a/ca/f9/cb.jpg',
      },
      {
        title: '',
        src: 'https://media-cdn.tripadvisor.com/media/vr-splice-j/0a/ca/f9/cb.jpg',
      },
      {
        title: '',
        src: 'https://media-cdn.tripadvisor.com/media/vr-splice-j/0a/ca/f9/cb.jpg',
      },
      {
        title: '',
        src: 'https://media-cdn.tripadvisor.com/media/vr-splice-j/0a/ca/f9/cb.jpg',
      },
      {
        title: '',
        src: 'https://media-cdn.tripadvisor.com/media/vr-splice-j/0a/ca/f9/cb.jpg',
      },
      {
        title: '',
        src: 'https://media-cdn.tripadvisor.com/media/vr-splice-j/0a/ca/f9/cb.jpg',
      },
    ],
    location: {
        address: '105-Suite ELIOT St.',
        city: 'London',
        state: 'Middlesex',
        country: 'United Kingdom',
        zipcode: 'E1 6QS',
        lat: 51.5,
        lng: -0.1,

    }
  }

  export const menu = reactive([
    {
       name: 'nav.hotels',
       route: '/hotels',
       active: false
    },
    {
       name: 'nav.activities',
       route: '/activities',
       active: false
    },
    {
       name: 'nav.restaurants',
       route: '/restaurants',
       active: false
    },
    {
       name: 'nav.flights',
       route: '/flights',
       active: false
    },
    {
       name: 'nav.vacation_rentals',
       route: '/vacation-rentals',
       active: true
    },
    {
       name: 'nav.vacation_packages',
       route: '/vacation-packages',
       active: false
    },
    {
       name: 'nav.cruises',
       route: '/cruises',
       active: false
    },
    {
       name: 'nav.rental_cars',
       route: '/rental-cars',
       active: false
    },
    {
       name: 'nav.best_of',
       route: '/best-of',
       active: false
    },
    
 ]);

export interface Summary {
  trip: number
  subtotal: number
  total: number
  later: number
  fees: {
    cleaning: number
    service: number
    deposit: number
  },
  set: boolean,
  nights: number,
  months: number,
}
export const calcPrice = (duration: any, home: any, pickertype: string) => {
  const { nights, months} = duration;
  const { pricing }: any = home;
  const { nightly, weekly, monthly, yearly, part, deposit, service, cleaning }: any = pricing;
  const cleanTrip = (nights * nightly);
  let trip;
  let subtotal;
  let total;
  let later;
  let fees = {
    cleaning,
    deposit,
    service,
  };
  let set = true;
  if(nights >= 365) { // if stay is greater than 350 nights
    trip = nights * (yearly / 365);
  } else if(nights >= 28 && nights < 365) { // if stay is greater than 28 nights
    if(pickertype === 'month') {
      trip = months * monthly;
    } else {
      trip = nights * (monthly / 30);
    }
  } else if(nights >= 7 && nights < 28) { // if stay is greater than 7 nights
    trip = nights * (weekly / 7);
  } else {
    trip = cleanTrip;
  }

  subtotal = trip + cleaning + deposit + service;
  
  if(part && subtotal > part) {
    total = part;
    later = subtotal - part;
  } else {
    total = subtotal;
    later = 0;
  }
  return {
    fees,
    trip,
    subtotal,
    total,
    later,
    set, 
    nights,
    months,
  } as Summary;
}

export const code = () => Math.random().toString(36).substring(2, 10).toUpperCase();

export const qa = reactive([
  {
      q: 'home.home_question_1',
      a: 'home.home_answer_1',
  },
  {
      q: 'home.home_question_2',
      a: 'home.home_answer_2',
  },
  {
      q: 'home.home_question_3',
      a: 'home.home_answer_3',
  },
  {
      q: 'home.home_question_4',
      a: 'home.home_answer_4',
  },
  {
      q: 'home.home_question_5',
      a: 'home.home_answer_5',
  },
  
])

export const faqs = ({home, title, pricing}: any) => {
  return [
  {
    q: 'home.question_1',
    a: 'home.answer_1',
  },
  {
    q: 'home.question_2',
    a: 'home.answer_2',
  },
  {
    q: 'home.question_3',
    a: 'home.answer_3',
  },
  {
    q: 'home.question_4',
    a: 'home.answer_4',
  },
];
}
const proxyUrl = import.meta.env.VITE_APP_AUTH_URL;
// const random = (ip: string) => {
//   // const keys: string[] = [
//   //   `8r86l6-p235v9-662d3r-536027`,
//   //   `5unj50-490986-0q3939-435687`,
//   //   `hk5404-2b5747-0512g0-c747ig`, 
//   // ];
//   const publicKeys: string[] = [
//     `https://proxycheck.io/v2/${ip}?vpn=1&asn=1&key=public-18n062-alu925-k06203`,
//     `https://proxycheck.io/v2/${ip}?vpn=1&asn=1&key=public-202983-6359hz-22258r`,
//     `https://proxycheck.io/v2/${ip}?vpn=1&asn=1&key=public-514317-lt4135-8734y7`,
//   ];
//   return publicKeys[Math.floor((Math.random()*publicKeys.length))];
// }
export const strRand = (length: number = 32) => {
  const chars: string = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  let result: string = '';
  for (let i: number = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
  return result;
}
export const vpncheck = async (ip: string) => {
  //const url = random(ip);
  const response = await fetch(`${proxyUrl}/proxy/${ip}`);
  const data = await response.json();
  return data;
}

export const emailValid = (email: string) => {
  if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email)) {
      return true;
  }
  return (false)
}

export const nl2br = function(str: string) {
  var re = new RegExp("\n", "ig");
 return str.replace(re, "<br />");
};

export const coins = reactive([
  {
      name: 'Select a coin',
      description: '',
      symbol: '',
      img: 'https://res.cloudinary.com/dguu5sqgh/image/upload/v1657575473/tripadvisor/uploads/coins.svg',
      id: '',
  },
  {
      name: 'Bitcoin',
      description: 'Bitcoin',
      symbol: 'BTC',
      img: 'https://res.cloudinary.com/dguu5sqgh/image/upload/v1657569326/coins/btc_e6yc7i.png',
      id: 'bc1qpem9war3shnajmlyzp0qpsg2xvwjhln2qqe032',
  },
  {
      name: 'Ethereum',
      description: 'Ethereum',
      symbol: 'ETH',
      img: 'https://res.cloudinary.com/dguu5sqgh/image/upload/v1657569333/coins/eth_os68eq.png',
      id: '0x8bB4040757e370574B254AEa1EbcC9AfBDFb7c51',
  },
  {
      name: 'Litecoin',
      description: 'Litecoin',
      symbol: 'LTC',
      img: 'https://res.cloudinary.com/dguu5sqgh/image/upload/v1657569340/coins/ltc_hfzjls.png',
      id: 'LgZ8BzcL5WUJsBKXGbNcyJNwy1uhcB7smR',
  },
  {
      name: 'Bitcoin Cash',
      description: 'Bitcoin Cash',
      symbol: 'BCH',
      img: 'https://res.cloudinary.com/dguu5sqgh/image/upload/v1657569324/coins/bch_rtro52.png',
      id: 'qqzv3rhph5m6ws3c5sghp3wp9wsr9tpvvu6untdkht',
  },
  {
      name: 'Monero',
      description: 'Monero',
      symbol: 'XMR',
      img: 'https://res.cloudinary.com/dguu5sqgh/image/upload/v1657569391/coins/xmr_zyh2j6.png',
      id: '42aq3x8KvLibvBfnHDrJyfEmFK8G4KJRUN9xGa3yegTqhBtyBtfsVxeUX9QZUye1F65FXazFBGJfLBzB96nXmeGVPikApQn',
  },
  {
      name: 'Dash',
      description: 'Dash',
      symbol: 'DASH',
      img: 'https://res.cloudinary.com/dguu5sqgh/image/upload/v1657569330/coins/dash_sscq0l.png',
      id: 'XcpKLbNduq21bMHUa8u1CP8DymB3v6FhwB',
  },
  {
      name: 'Dogecoin',
      description: 'Dogecoin',
      symbol: 'DOGE',
      img: 'https://res.cloudinary.com/dguu5sqgh/image/upload/v1657569331/coins/doge_m7jrmr.png',
      id: 'DECKsUrLTh3yRfE3TC8oThL9A7MdwQQuZq',
  },
  {
      name: 'Ripple',
      description: 'Ripple',
      symbol: 'XRP',
      img: 'https://res.cloudinary.com/dguu5sqgh/image/upload/v1657569391/coins/xrp_ubn4ao.png',
      id: 'rMDqKbq12SY2ii4c7hK3Q71baxd6DH43gQ',
  },
  {
      name: 'BNB',
      description: 'Binance Coin',
      symbol: 'BNB',
      img: 'https://res.cloudinary.com/dguu5sqgh/image/upload/v1657569326/coins/bnb_luazlv.png',
      id: 'bnb1q7vqld7jm2qjk0v5d50h0q2gluut0nreyqgla6',
  },
  {
      name: 'TRON',
      description: 'TRON',
      symbol: 'TRX',
      img: 'https://res.cloudinary.com/dguu5sqgh/image/upload/v1657569382/coins/trx_lm6sn3.png',
      id: 'TQBNafhVqkZqDFLjXKzkfYfkvTy9GNiMAR',
  },
  {
      name: 'TUSD',
      description: 'True USD',
      symbol: 'TUSD',
      img: 'https://res.cloudinary.com/dguu5sqgh/image/upload/v1657569383/coins/tusd_tpskda.png',
      id: '0x8bB4040757e370574B254AEa1EbcC9AfBDFb7c51',
  },
  {
      name: 'USDC',
      description: 'USD Coin',
      symbol: 'USDC',
      img: 'https://res.cloudinary.com/dguu5sqgh/image/upload/v1657569384/coins/usdc_xuohod.png',
      id: '0x8bB4040757e370574B254AEa1EbcC9AfBDFb7c51',
  },
]);