export default [
  {
    id: 1001,
    name: "Non-refundable",
    text:
      "Cancel before check-in and get back only the cleaning fee, if you paid one.",
  },
  {
    id: 1002,
    name: "Flexible",
    text:
      "Free cancellation until 24 hours before check-in (time shown in the confirmation email). After that, cancel before check-in and get a full refund, minus the first night and service fee.",
  },
  {
    id: 1003,
    name: "Moderate",
    text:
      "Free cancellation until 5 days before check-in (time shown in the confirmation email). After that, cancel before check-in and get a 50% refund, minus the first night and service fee.",
  },
  {
    id: 1004,
    name: "Strict",
    text:
      "Free cancellation for 48 hours, as long as the guest cancels at least 14 days before check-in (time shown in the confirmation email). After that, guests can cancel up to 7 days before check-in and get a 50% refund of the nightly rate, and the cleaning fee, but not the service fee.",
  },
  {
    id: 1005,
    name: "Long Term",
    text:
      "Automatically applied to reservations of 28 nights or more. Reservations are fully refundable for 48 hours after the booking is confirmed, as long as the cancellation occurs at least 28 days before check-in (3:00 PM in the destination’s local time if not specified). More than 48 hours after booking, guests can cancel before check-in and get a full refund, minus the first 30 days and the service fee.",
  },
  {
    id: 1006,
    name: "Super Strict 30 Days",
    text:
      "Guests can cancel at least 30 days before check-in and get a 50% refund of the nightly rate and the cleaning fee, but not the service fee. The Airbnb service fee is not refundable. This policy is only available to software-connected hosts.",
  },
  {
    id: 1007,
    name: "Super Strict 60 Days",
    text:
      "Guests can cancel at least 60 days before check-in and get a 50% refund of the nightly rate and the cleaning fee, but not the service fee. The Airbnb service fee is not refundable. This policy is only available to software-connected hosts.",
  },
];