const regions: object[] = [
	{ "name": "Africa", "code": "AF" },
	{ "name": "North America", "code": "NA" },
	{ "name": "Oceania", "code": "OC" },
	{ "name": "Antarctica", "code": "AN" },
	{ "name": "Asia", "code": "AS" },
	{ "name": "Europe", "code": "EU" },
	{ "name": "South America", "code": "SA" },
];

const countries: object[] = [
    {
        "name": "Afghanistan",
        "code": "AF",
        "capital": "Kabul",
        "region": "Asia",
        "currency": {
            "code": "AFN",
            "name": "Afghan afghani",
            "symbol": "؋"
        },
        "language": {
            "code": "ps",
            "name": "Pashto"
        },
        "flag": "https://restcountries.eu/data/afg.svg"
    },
    {
        "name": "Åland Islands",
        "code": "AX",
        "capital": "Mariehamn",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "sv",
            "name": "Swedish"
        },
        "flag": "https://restcountries.eu/data/ala.svg"
    },
    {
        "name": "Albania",
        "code": "AL",
        "capital": "Tirana",
        "region": "Europe",
        "currency": {
            "code": "ALL",
            "name": "Albanian lek",
            "symbol": "L"
        },
        "language": {
            "code": "sq",
            "name": "Albanian"
        },
        "flag": "https://restcountries.eu/data/alb.svg"
    },
    {
        "name": "Algeria",
        "code": "DZ",
        "capital": "Algiers",
        "region": "Africa",
        "currency": {
            "code": "DZD",
            "name": "Algerian dinar",
            "symbol": "د.ج"
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/dza.svg"
    },
    {
        "name": "American Samoa",
        "code": "AS",
        "capital": "Pago Pago",
        "region": "Oceania",
        "currency": {
            "code": "USD",
            "name": "United State Dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/asm.svg"
    },
    {
        "name": "Andorra",
        "code": "AD",
        "capital": "Andorra la Vella",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "ca",
            "name": "Catalan"
        },
        "flag": "https://restcountries.eu/data/and.svg"
    },
    {
        "name": "Angola",
        "code": "AO",
        "capital": "Luanda",
        "region": "Africa",
        "currency": {
            "code": "AOA",
            "name": "Angolan kwanza",
            "symbol": "Kz"
        },
        "language": {
            "code": "pt",
            "name": "Portuguese"
        },
        "flag": "https://restcountries.eu/data/ago.svg"
    },
    {
        "name": "Anguilla",
        "code": "AI",
        "capital": "The Valley",
        "region": "North America",
        "currency": {
            "code": "XCD",
            "name": "East Caribbean dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/aia.svg"
    },
    {
        "name": "Antigua and Barbuda",
        "code": "AG",
        "capital": "Saint John's",
        "region": "North America",
        "currency": {
            "code": "XCD",
            "name": "East Caribbean dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/atg.svg"
    },
    {
        "name": "Argentina",
        "code": "AR",
        "capital": "Buenos Aires",
        "region": "South America",
        "currency": {
            "code": "ARS",
            "name": "Argentine peso",
            "symbol": "$"
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/arg.svg"
    },
    {
        "name": "Armenia",
        "code": "AM",
        "capital": "Yerevan",
        "region": "Asia",
        "currency": {
            "code": "AMD",
            "name": "Armenian dram",
            "symbol": null
        },
        "language": {
            "code": "hy",
            "name": "Armenian"
        },
        "flag": "https://restcountries.eu/data/arm.svg"
    },
    {
        "name": "Aruba",
        "code": "AW",
        "capital": "Oranjestad",
        "region": "South America",
        "currency": {
            "code": "AWG",
            "name": "Aruban florin",
            "symbol": "ƒ"
        },
        "language": {
            "code": "nl",
            "name": "Dutch"
        },
        "flag": "https://restcountries.eu/data/abw.svg"
    },
    {
        "name": "Australia",
        "code": "AU",
        "capital": "Canberra",
        "region": "Oceania",
        "currency": {
            "code": "AUD",
            "name": "Australian dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/aus.svg"
    },
    {
        "name": "Austria",
        "code": "AT",
        "capital": "Vienna",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "de",
            "name": "German"
        },
        "flag": "https://restcountries.eu/data/aut.svg"
    },
    {
        "name": "Azerbaijan",
        "code": "AZ",
        "capital": "Baku",
        "region": "Asia",
        "currency": {
            "code": "AZN",
            "name": "Azerbaijani manat",
            "symbol": null
        },
        "language": {
            "code": "az",
            "name": "Azerbaijani"
        },
        "flag": "https://restcountries.eu/data/aze.svg"
    },
    {
        "name": "Bahamas",
        "code": "BS",
        "capital": "Nassau",
        "region": "North America",
        "currency": {
            "code": "BSD",
            "name": "Bahamian dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/bhs.svg"
    },
    {
        "name": "Bahrain",
        "code": "BH",
        "capital": "Manama",
        "region": "Asia",
        "currency": {
            "code": "BHD",
            "name": "Bahraini dinar",
            "symbol": ".د.ب"
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/bhr.svg"
    },
    {
        "name": "Bangladesh",
        "code": "BD",
        "capital": "Dhaka",
        "region": "Asia",
        "currency": {
            "code": "BDT",
            "name": "Bangladeshi taka",
            "symbol": "৳"
        },
        "language": {
            "code": "bn",
            "name": "Bengali"
        },
        "flag": "https://restcountries.eu/data/bgd.svg"
    },
    {
        "name": "Barbados",
        "code": "BB",
        "capital": "Bridgetown",
        "region": "North America",
        "currency": {
            "code": "BBD",
            "name": "Barbadian dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/brb.svg"
    },
    {
        "name": "Belarus",
        "code": "BY",
        "capital": "Minsk",
        "region": "Europe",
        "currency": {
            "code": "BYN",
            "name": "New Belarusian ruble",
            "symbol": "Br"
        },
        "language": {
            "code": "be",
            "name": "Belarusian"
        },
        "flag": "https://restcountries.eu/data/blr.svg"
    },
    {
        "name": "Belgium",
        "code": "BE",
        "capital": "Brussels",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "nl",
            "name": "Dutch"
        },
        "flag": "https://restcountries.eu/data/bel.svg"
    },
    {
        "name": "Belize",
        "code": "BZ",
        "capital": "Belmopan",
        "region": "North America",
        "currency": {
            "code": "BZD",
            "name": "Belize dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/blz.svg"
    },
    {
        "name": "Benin",
        "code": "BJ",
        "capital": "Porto-Novo",
        "region": "Africa",
        "currency": {
            "code": "XOF",
            "name": "West African CFA franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/ben.svg"
    },
    {
        "name": "Bermuda",
        "code": "BM",
        "capital": "Hamilton",
        "region": "North America",
        "currency": {
            "code": "BMD",
            "name": "Bermudian dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/bmu.svg"
    },
    {
        "name": "Bhutan",
        "code": "BT",
        "capital": "Thimphu",
        "region": "Asia",
        "currency": {
            "code": "BTN",
            "name": "Bhutanese ngultrum",
            "symbol": "Nu."
        },
        "language": {
            "code": "dz",
            "name": "Dzongkha"
        },
        "flag": "https://restcountries.eu/data/btn.svg"
    },
    {
        "name": "Bolivia (Plurinational State of)",
        "code": "BO",
        "capital": "Sucre",
        "region": "South America",
        "currency": {
            "code": "BOB",
            "name": "Bolivian boliviano",
            "symbol": "Bs."
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/bol.svg"
    },
    {
        "name": "Bonaire, Sint Eustatius and Saba",
        "code": "BQ",
        "capital": "Kralendijk",
        "region": "South America",
        "currency": {
            "code": "USD",
            "name": "United States dollar",
            "symbol": "$"
        },
        "language": {
            "code": "nl",
            "name": "Dutch"
        },
        "flag": "https://restcountries.eu/data/bes.svg"
    },
    {
        "name": "Bosnia and Herzegovina",
        "code": "BA",
        "capital": "Sarajevo",
        "region": "Europe",
        "currency": {
            "code": "BAM",
            "name": "Bosnia and Herzegovina convertible mark",
            "symbol": null
        },
        "language": {
            "code": "bs",
            "name": "Bosnian"
        },
        "flag": "https://restcountries.eu/data/bih.svg"
    },
    {
        "name": "Botswana",
        "code": "BW",
        "capital": "Gaborone",
        "region": "Africa",
        "currency": {
            "code": "BWP",
            "name": "Botswana pula",
            "symbol": "P"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/bwa.svg"
    },
    {
        "name": "Bouvet Island",
        "code": "BV",
        "capital": "",
        "region": "Antarctica",
        "currency": {
            "code": "NOK",
            "name": "Norwegian krone",
            "symbol": "kr"
        },
        "language": {
            "code": "no",
            "name": "Norwegian"
        },
        "flag": "https://restcountries.eu/data/bvt.svg"
    },
    {
        "name": "Brazil",
        "code": "BR",
        "capital": "Brasília",
        "region": "South America",
        "currency": {
            "code": "BRL",
            "name": "Brazilian real",
            "symbol": "R$"
        },
        "language": {
            "code": "pt",
            "name": "Portuguese"
        },
        "flag": "https://restcountries.eu/data/bra.svg"
    },
    {
        "name": "British Indian Ocean Territory",
        "code": "IO",
        "capital": "Diego Garcia",
        "region": "Africa",
        "currency": {
            "code": "USD",
            "name": "United States dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/iot.svg"
    },
    {
        "name": "United States Minor Outlying Islands",
        "code": "UM",
        "capital": "",
        "region": "North America",
        "currency": {
            "code": "USD",
            "name": "United States Dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/umi.svg"
    },
    {
        "name": "Virgin Islands (British)",
        "code": "VG",
        "capital": "Road Town",
        "region": "North America",
        "currency": {
            "code": "USD",
            "name": "United States dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/vgb.svg"
    },
    {
        "name": "Virgin Islands (U.S.)",
        "code": "VI",
        "capital": "Charlotte Amalie",
        "region": "North America",
        "currency": {
            "code": "USD",
            "name": "United States dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/vir.svg"
    },
    {
        "name": "Brunei Darussalam",
        "code": "BN",
        "capital": "Bandar Seri Begawan",
        "region": "Asia",
        "currency": {
            "code": "BND",
            "name": "Brunei dollar",
            "symbol": "$"
        },
        "language": {
            "code": "ms",
            "name": "Malay"
        },
        "flag": "https://restcountries.eu/data/brn.svg"
    },
    {
        "name": "Bulgaria",
        "code": "BG",
        "capital": "Sofia",
        "region": "Europe",
        "currency": {
            "code": "BGN",
            "name": "Bulgarian lev",
            "symbol": "лв"
        },
        "language": {
            "code": "bg",
            "name": "Bulgarian"
        },
        "flag": "https://restcountries.eu/data/bgr.svg"
    },
    {
        "name": "Burkina Faso",
        "code": "BF",
        "capital": "Ouagadougou",
        "region": "Africa",
        "currency": {
            "code": "XOF",
            "name": "West African CFA franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/bfa.svg"
    },
    {
        "name": "Burundi",
        "code": "BI",
        "capital": "Bujumbura",
        "region": "Africa",
        "currency": {
            "code": "BIF",
            "name": "Burundian franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/bdi.svg"
    },
    {
        "name": "Cambodia",
        "code": "KH",
        "capital": "Phnom Penh",
        "region": "Asia",
        "currency": {
            "code": "KHR",
            "name": "Cambodian riel",
            "symbol": "៛"
        },
        "language": {
            "code": "km",
            "name": "Khmer"
        },
        "flag": "https://restcountries.eu/data/khm.svg"
    },
    {
        "name": "Cameroon",
        "code": "CM",
        "capital": "Yaoundé",
        "region": "Africa",
        "currency": {
            "code": "XAF",
            "name": "Central African CFA franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/cmr.svg"
    },
    {
        "name": "Canada",
        "code": "CA",
        "capital": "Ottawa",
        "region": "North America",
        "currency": {
            "code": "CAD",
            "name": "Canadian dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/can.svg"
    },
    {
        "name": "Cabo Verde",
        "code": "CV",
        "capital": "Praia",
        "region": "Africa",
        "currency": {
            "code": "CVE",
            "name": "Cape Verdean escudo",
            "symbol": "Esc"
        },
        "language": {
            "code": "pt",
            "iso639_2": "por",
            "name": "Portuguese",
            "nativeName": "Português"
        },
        "flag": "https://restcountries.eu/data/cpv.svg"
    },
    {
        "name": "Cayman Islands",
        "code": "KY",
        "capital": "George Town",
        "region": "North America",
        "demonym": "Caymanian",
        "currency": {
            "code": "KYD",
            "name": "Cayman Islands dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/cym.svg"
    },
    {
        "name": "Central African Republic",
        "code": "CF",
        "capital": "Bangui",
        "region": "Africa",
        "currency": {
            "code": "XAF",
            "name": "Central African CFA franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/caf.svg"
    },
    {
        "name": "Chad",
        "code": "TD",
        "capital": "N'Djamena",
        "region": "Africa",
        "currency": {
            "code": "XAF",
            "name": "Central African CFA franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/tcd.svg"
    },
    {
        "name": "Chile",
        "code": "CL",
        "capital": "Santiago",
        "region": "South America",
        "currency": {
            "code": "CLP",
            "name": "Chilean peso",
            "symbol": "$"
        },
        "language": {
            "code": "es",
            "iso639_2": "spa",
            "name": "Spanish",
            "nativeName": "Español"
        },
        "flag": "https://restcountries.eu/data/chl.svg"
    },
    {
        "name": "China",
        "code": "CN",
        "capital": "Beijing",
        "region": "Asia",
        "currency": {
            "code": "CNY",
            "name": "Chinese yuan",
            "symbol": "¥"
        },
        "language": {
            "code": "zh",
            "name": "Chinese"
        },
        "flag": "https://restcountries.eu/data/chn.svg"
    },
    {
        "name": "Christmas Island",
        "code": "CX",
        "capital": "Flying Fish Cove",
        "region": "Oceania",
        "currency": {
            "code": "AUD",
            "name": "Australian dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/cxr.svg"
    },
    {
        "name": "Cocos (Keeling) Islands",
        "code": "CC",
        "capital": "West Island",
        "region": "Oceania",
        "currency": {
            "code": "AUD",
            "name": "Australian dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/cck.svg"
    },
    {
        "name": "Colombia",
        "code": "CO",
        "capital": "Bogotá",
        "region": "South America",
        "currency": {
            "code": "COP",
            "name": "Colombian peso",
            "symbol": "$"
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/col.svg"
    },
    {
        "name": "Comoros",
        "code": "KM",
        "capital": "Moroni",
        "region": "Africa",
        "currency": {
            "code": "KMF",
            "name": "Comorian franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/com.svg"
    },
    {
        "name": "Congo",
        "code": "CG",
        "capital": "Brazzaville",
        "region": "Africa",
        "currency": {
            "code": "XAF",
            "name": "Central African CFA franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/cog.svg"
    },
    {
        "name": "Congo (Democratic Republic of the)",
        "code": "CD",
        "capital": "Kinshasa",
        "region": "Africa",
        "currency": {
            "code": "CDF",
            "name": "Congolese franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/cod.svg"
    },
    {
        "name": "Cook Islands",
        "code": "CK",
        "capital": "Avarua",
        "region": "Oceania",
        "currency": {
            "code": "NZD",
            "name": "New Zealand dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/cok.svg"
    },
    {
        "name": "Costa Rica",
        "code": "CR",
        "capital": "San José",
        "region": "North America",
        "currency": {
            "code": "CRC",
            "name": "Costa Rican colón",
            "symbol": "₡"
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/cri.svg"
    },
    {
        "name": "Croatia",
        "code": "HR",
        "capital": "Zagreb",
        "region": "Europe",
        "currency": {
            "code": "HRK",
            "name": "Croatian kuna",
            "symbol": "kn"
        },
        "language": {
            "code": "hr",
            "name": "Croatian"
        },
        "flag": "https://restcountries.eu/data/hrv.svg"
    },
    {
        "name": "Cuba",
        "code": "CU",
        "capital": "Havana",
        "region": "North America",
        "currency": {
            "code": "CUC",
            "name": "Cuban convertible peso",
            "symbol": "$"
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/cub.svg"
    },
    {
        "name": "Curaçao",
        "code": "CW",
        "capital": "Willemstad",
        "region": "South America",
        "currency": {
            "code": "ANG",
            "name": "Netherlands Antillean guilder",
            "symbol": "ƒ"
        },
        "language": {
            "code": "nl",
            "name": "Dutch"
        },
        "flag": "https://restcountries.eu/data/cuw.svg"
    },
    {
        "name": "Cyprus",
        "code": "CY",
        "capital": "Nicosia",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "tr",
            "name": "Turkish"
        },
        "flag": "https://restcountries.eu/data/cyp.svg"
    },
    {
        "name": "Czech Republic",
        "code": "CZ",
        "capital": "Prague",
        "region": "Europe",
        "currency": {
            "code": "CZK",
            "name": "Czech koruna",
            "symbol": "Kč"
        },
        "language": {
            "code": "cs",
            "name": "Czech"
        },
        "flag": "https://restcountries.eu/data/cze.svg"
    },
    {
        "name": "Czechia",
        "code": "CZ",
        "capital": "Prague",
        "region": "Europe",
        "currency": {
            "code": "CZK",
            "name": "Czech koruna",
            "symbol": "Kč"
        },
        "language": {
            "code": "cs",
            "name": "Czech"
        },
        "flag": "https://restcountries.eu/data/cze.svg"
    },
    {
        "name": "Denmark",
        "code": "DK",
        "capital": "Copenhagen",
        "region": "Europe",
        "currency": {
            "code": "DKK",
            "name": "Danish krone",
            "symbol": "kr"
        },
        "language": {
            "code": "da",
            "name": "Danish"
        },
        "flag": "https://restcountries.eu/data/dnk.svg"
    },
    {
        "name": "Djibouti",
        "code": "DJ",
        "capital": "Djibouti",
        "region": "Africa",
        "currency": {
            "code": "DJF",
            "name": "Djiboutian franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/dji.svg"
    },
    {
        "name": "Dominica",
        "code": "DM",
        "capital": "Roseau",
        "region": "North America",
        "currency": {
            "code": "XCD",
            "name": "East Caribbean dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/dma.svg"
    },
    {
        "name": "Dominican Republic",
        "code": "DO",
        "capital": "Santo Domingo",
        "region": "North America",
        "currency": {
            "code": "DOP",
            "name": "Dominican peso",
            "symbol": "$"
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/dom.svg"
    },
    {
        "name": "Ecuador",
        "code": "EC",
        "capital": "Quito",
        "region": "South America",
        "currency": {
            "code": "USD",
            "name": "United States dollar",
            "symbol": "$"
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/ecu.svg"
    },
    {
        "name": "Egypt",
        "code": "EG",
        "capital": "Cairo",
        "region": "Africa",
        "currency": {
            "code": "EGP",
            "name": "Egyptian pound",
            "symbol": "£"
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/egy.svg"
    },
    {
        "name": "El Salvador",
        "code": "SV",
        "capital": "San Salvador",
        "region": "North America",
        "currency": {
            "code": "USD",
            "name": "United States dollar",
            "symbol": "$"
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/slv.svg"
    },
    {
        "name": "Equatorial Guinea",
        "code": "GQ",
        "capital": "Malabo",
        "region": "Africa",
        "currency": {
            "code": "XAF",
            "name": "Central African CFA franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "es",
            "iso639_2": "spa",
            "name": "Spanish",
            "nativeName": "Español"
        },
        "flag": "https://restcountries.eu/data/gnq.svg"
    },
    {
        "name": "Eritrea",
        "code": "ER",
        "capital": "Asmara",
        "region": "Africa",
        "currency": {
            "code": "ERN",
            "name": "Eritrean nakfa",
            "symbol": "Nfk"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/eri.svg"
    },
    {
        "name": "Estonia",
        "code": "EE",
        "capital": "Tallinn",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "et",
            "name": "Estonian"
        },
        "flag": "https://restcountries.eu/data/est.svg"
    },
    {
        "name": "Ethiopia",
        "code": "ET",
        "capital": "Addis Ababa",
        "region": "Africa",
        "currency": {
            "code": "ETB",
            "name": "Ethiopian birr",
            "symbol": "Br"
        },
        "language": {
            "code": "am",
            "name": "Amharic"
        },
        "flag": "https://restcountries.eu/data/eth.svg"
    },
    {
        "name": "Falkland Islands (Malvinas)",
        "code": "FK",
        "capital": "Stanley",
        "region": "South America",
        "currency": {
            "code": "FKP",
            "name": "Falkland Islands pound",
            "symbol": "£"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/flk.svg"
    },
    {
        "name": "Faroe Islands",
        "code": "FO",
        "capital": "Tórshavn",
        "region": "Europe",
        "currency": {
            "code": "DKK",
            "name": "Danish krone",
            "symbol": "kr"
        },
        "language": {
            "code": "fo",
            "name": "Faroese"
        },
        "flag": "https://restcountries.eu/data/fro.svg"
    },
    {
        "name": "Fiji",
        "code": "FJ",
        "capital": "Suva",
        "region": "Oceania",
        "currency": {
            "code": "FJD",
            "name": "Fijian dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/fji.svg"
    },
    {
        "name": "Finland",
        "code": "FI",
        "capital": "Helsinki",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "fi",
            "iso639_2": "fin",
            "name": "Finnish",
            "nativeName": "suomi"
        },
        "flag": "https://restcountries.eu/data/fin.svg"
    },
    {
        "name": "France",
        "code": "FR",
        "capital": "Paris",
        "region": "Europe",
        "demonym": "French",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/fra.svg"
    },
    {
        "name": "French Guiana",
        "code": "GF",
        "capital": "Cayenne",
        "region": "South America",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/guf.svg"
    },
    {
        "name": "French Polynesia",
        "code": "PF",
        "capital": "Papeetē",
        "region": "Oceania",
        "currency": {
            "code": "XPF",
            "name": "CFP franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/pyf.svg"
    },
    {
        "name": "French Southern Territories",
        "code": "TF",
        "capital": "Port-aux-Français",
        "region": "Africa",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/atf.svg"
    },
    {
        "name": "Gabon",
        "code": "GA",
        "capital": "Libreville",
        "region": "Africa",
        "currency": {
            "code": "XAF",
            "name": "Central African CFA franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/gab.svg"
    },
    {
        "name": "Gambia",
        "code": "GM",
        "capital": "Banjul",
        "region": "Africa",
        "currency": {
            "code": "GMD",
            "name": "Gambian dalasi",
            "symbol": "D"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/gmb.svg"
    },
    {
        "name": "Georgia",
        "code": "GE",
        "capital": "Tbilisi",
        "region": "Asia",
        "currency": {
            "code": "GEL",
            "name": "Georgian Lari",
            "symbol": "ლ"
        },
        "language": {
            "code": "ka",
            "name": "Georgian"
        },
        "flag": "https://restcountries.eu/data/geo.svg"
    },
    {
        "name": "Germany",
        "code": "DE",
        "capital": "Berlin",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "de",
            "name": "German"
        },
        "flag": "https://restcountries.eu/data/deu.svg"
    },
    {
        "name": "Ghana",
        "code": "GH",
        "capital": "Accra",
        "region": "Africa",
        "currency": {
            "code": "GHS",
            "name": "Ghanaian cedi",
            "symbol": "₵"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/gha.svg"
    },
    {
        "name": "Gibraltar",
        "code": "GI",
        "capital": "Gibraltar",
        "region": "Europe",
        "currency": {
            "code": "GIP",
            "name": "Gibraltar pound",
            "symbol": "£"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/gib.svg"
    },
    {
        "name": "Greece",
        "code": "GR",
        "capital": "Athens",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "el",
            "name": "Greek (modern)"
        },
        "flag": "https://restcountries.eu/data/grc.svg"
    },
    {
        "name": "Greenland",
        "code": "GL",
        "capital": "Nuuk",
        "region": "North America",
        "currency": {
            "code": "DKK",
            "name": "Danish krone",
            "symbol": "kr"
        },
        "language": {
            "code": "kl",
            "name": "Kalaallisut"
        },
        "flag": "https://restcountries.eu/data/grl.svg"
    },
    {
        "name": "Grenada",
        "code": "GD",
        "capital": "St. George's",
        "region": "North America",
        "currency": {
            "code": "XCD",
            "name": "East Caribbean dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/grd.svg"
    },
    {
        "name": "Guadeloupe",
        "code": "GP",
        "capital": "Basse-Terre",
        "region": "North America",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/glp.svg"
    },
    {
        "name": "Guam",
        "code": "GU",
        "capital": "Hagåtña",
        "region": "Oceania",
        "currency": {
            "code": "USD",
            "name": "United States dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/gum.svg"
    },
    {
        "name": "Guatemala",
        "code": "GT",
        "capital": "Guatemala City",
        "region": "North America",
        "currency": {
            "code": "GTQ",
            "name": "Guatemalan quetzal",
            "symbol": "Q"
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/gtm.svg"
    },
    {
        "name": "Guernsey",
        "code": "GG",
        "capital": "St. Peter Port",
        "region": "Europe",
        "currency": {
            "code": "GBP",
            "name": "British pound",
            "symbol": "£"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/ggy.svg"
    },
    {
        "name": "Guinea",
        "code": "GN",
        "capital": "Conakry",
        "region": "Africa",
        "currency": {
            "code": "GNF",
            "name": "Guinean franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/gin.svg"
    },
    {
        "name": "Guinea-Bissau",
        "code": "GW",
        "capital": "Bissau",
        "region": "Africa",
        "currency": {
            "code": "XOF",
            "name": "West African CFA franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "pt",
            "name": "Portuguese"
        },
        "flag": "https://restcountries.eu/data/gnb.svg"
    },
    {
        "name": "Guyana",
        "code": "GY",
        "capital": "Georgetown",
        "region": "South America",
        "currency": {
            "code": "GYD",
            "name": "Guyanese dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/guy.svg"
    },
    {
        "name": "Haiti",
        "code": "HT",
        "capital": "Port-au-Prince",
        "region": "Americas",
        "currency": {
            "code": "HTG",
            "name": "Haitian gourde",
            "symbol": "G"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/hti.svg"
    },
    {
        "name": "Heard Island and McDonald Islands",
        "code": "HM",
        "capital": "",
        "region": "",
        "currency": {
            "code": "AUD",
            "name": "Australian dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/hmd.svg"
    },
    {
        "name": "Holy See",
        "code": "VA",
        "capital": "Rome",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/vat.svg"
    },
    {
        "name": "Honduras",
        "code": "HN",
        "capital": "Tegucigalpa",
        "region": "North America",
        "currency": {
            "code": "HNL",
            "name": "Honduran lempira",
            "symbol": "L"
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/hnd.svg"
    },
    {
        "name": "Hong Kong",
        "code": "HK",
        "capital": "City of Victoria",
        "region": "Asia",
        "currency": {
            "code": "HKD",
            "name": "Hong Kong dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/hkg.svg"
    },
    {
        "name": "Hungary",
        "code": "HU",
        "capital": "Budapest",
        "region": "Europe",
        "currency": {
            "code": "HUF",
            "name": "Hungarian forint",
            "symbol": "Ft"
        },
        "language": {
            "code": "hu",
            "name": "Hungarian"
        },
        "flag": "https://restcountries.eu/data/hun.svg"
    },
    {
        "name": "Iceland",
        "code": "IS",
        "capital": "Reykjavík",
        "region": "Europe",
        "currency": {
            "code": "ISK",
            "name": "Icelandic króna",
            "symbol": "kr"
        },
        "language": {
            "code": "is",
            "name": "Icelandic"
        },
        "flag": "https://restcountries.eu/data/isl.svg"
    },
    {
        "name": "India",
        "code": "IN",
        "capital": "New Delhi",
        "region": "Asia",
        "currency": {
            "code": "INR",
            "name": "Indian rupee",
            "symbol": "₹"
        },
        "language": {
            "code": "hi",
            "name": "Hindi"
        },
        "flag": "https://restcountries.eu/data/ind.svg"
    },
    {
        "name": "Indonesia",
        "code": "ID",
        "capital": "Jakarta",
        "region": "Asia",
        "currency": {
            "code": "IDR",
            "name": "Indonesian rupiah",
            "symbol": "Rp"
        },
        "language": {
            "code": "id",
            "name": "Indonesian"
        },
        "flag": "https://restcountries.eu/data/idn.svg"
    },
    {
        "name": "Côte d'Ivoire",
        "code": "CI",
        "capital": "Yamoussoukro",
        "region": "Africa",
        "currency": {
            "code": "XOF",
            "name": "West African CFA franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/civ.svg"
    },
    {
        "name": "Iran (Islamic Republic of)",
        "code": "IR",
        "capital": "Tehran",
        "region": "Asia",
        "currency": {
            "code": "IRR",
            "name": "Iranian rial",
            "symbol": "﷼"
        },
        "language": {
            "code": "fa",
            "name": "Persian (Farsi)"
        },
        "flag": "https://restcountries.eu/data/irn.svg"
    },
    {
        "name": "Iraq",
        "code": "IQ",
        "capital": "Baghdad",
        "region": "Asia",
        "currency": {
            "code": "IQD",
            "name": "Iraqi dinar",
            "symbol": "ع.د"
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/irq.svg"
    },
    {
        "name": "Ireland",
        "code": "IE",
        "capital": "Dublin",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "ga",
            "name": "Irish"
        },
        "flag": "https://restcountries.eu/data/irl.svg"
    },
    {
        "name": "Isle of Man",
        "code": "IM",
        "capital": "Douglas",
        "region": "Europe",
        "currency": {
            "code": "GBP",
            "name": "British pound",
            "symbol": "£"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/imn.svg"
    },
    {
        "name": "Israel",
        "code": "IL",
        "capital": "Jerusalem",
        "region": "Asia",
        "currency": {
            "code": "ILS",
            "name": "Israeli new shekel",
            "symbol": "₪"
        },
        "language": {
            "code": "he",
            "name": "Hebrew (modern)"
        },
        "flag": "https://restcountries.eu/data/isr.svg"
    },
    {
        "name": "Italy",
        "code": "IT",
        "capital": "Rome",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "it",
            "name": "Italian"
        },
        "flag": "https://restcountries.eu/data/ita.svg"
    },
    {
        "name": "Jamaica",
        "code": "JM",
        "capital": "Kingston",
        "region": "North America",
        "currency": {
            "code": "JMD",
            "name": "Jamaican dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/jam.svg"
    },
    {
        "name": "Japan",
        "code": "JP",
        "capital": "Tokyo",
        "region": "Asia",
        "currency": {
            "code": "JPY",
            "name": "Japanese yen",
            "symbol": "¥"
        },
        "language": {
            "code": "ja",
            "name": "Japanese"
        },
        "flag": "https://restcountries.eu/data/jpn.svg"
    },
    {
        "name": "Jersey",
        "code": "JE",
        "capital": "Saint Helier",
        "region": "Europe",
        "currency": {
            "code": "GBP",
            "name": "British pound",
            "symbol": "£"
        },
        "language": {
            "code": "en",
            "iso639_2": "eng",
            "name": "English",
            "nativeName": "English"
        },
        "flag": "https://restcountries.eu/data/jey.svg"
    },
    {
        "name": "Jordan",
        "code": "JO",
        "capital": "Amman",
        "region": "Asia",
        "currency": {
            "code": "JOD",
            "name": "Jordanian dinar",
            "symbol": "د.ا"
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/jor.svg"
    },
    {
        "name": "Kazakhstan",
        "code": "KZ",
        "capital": "Astana",
        "region": "Asia",
        "currency": {
            "code": "KZT",
            "name": "Kazakhstani tenge",
            "symbol": null
        },
        "language": {
            "code": "kk",
            "name": "Kazakh"
        },
        "flag": "https://restcountries.eu/data/kaz.svg"
    },
    {
        "name": "Kenya",
        "code": "KE",
        "capital": "Nairobi",
        "region": "Africa",
        "currency": {
            "code": "KES",
            "name": "Kenyan shilling",
            "symbol": "Sh"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/ken.svg"
    },
    {
        "name": "Kiribati",
        "code": "KI",
        "capital": "South Tarawa",
        "region": "Oceania",
        "currency": {
            "code": "AUD",
            "name": "Australian dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/kir.svg"
    },
    {
        "name": "Kuwait",
        "code": "KW",
        "capital": "Kuwait City",
        "region": "Asia",
        "currency": {
            "code": "KWD",
            "name": "Kuwaiti dinar",
            "symbol": "د.ك"
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/kwt.svg"
    },
    {
        "name": "Kyrgyzstan",
        "code": "KG",
        "capital": "Bishkek",
        "region": "Asia",
        "currency": {
            "code": "KGS",
            "name": "Kyrgyzstani som",
            "symbol": "с"
        },
        "language": {
            "code": "ky",
            "name": "Kyrgyz"
        },
        "flag": "https://restcountries.eu/data/kgz.svg"
    },
    {
        "name": "Lao People's Democratic Republic",
        "code": "LA",
        "capital": "Vientiane",
        "region": "Asia",
        "currency": {
            "code": "LAK",
            "name": "Lao kip",
            "symbol": "₭"
        },
        "language": {
            "code": "lo",
            "name": "Lao"
        },
        "flag": "https://restcountries.eu/data/lao.svg"
    },
    {
        "name": "Latvia",
        "code": "LV",
        "capital": "Riga",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "lv",
            "name": "Latvian"
        },
        "flag": "https://restcountries.eu/data/lva.svg"
    },
    {
        "name": "Lebanon",
        "code": "LB",
        "capital": "Beirut",
        "region": "Asia",
        "currency": {
            "code": "LBP",
            "name": "Lebanese pound",
            "symbol": "ل.ل"
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/lbn.svg"
    },
    {
        "name": "Lesotho",
        "code": "LS",
        "capital": "Maseru",
        "region": "Africa",
        "currency": {
            "code": "LSL",
            "name": "Lesotho loti",
            "symbol": "L"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/lso.svg"
    },
    {
        "name": "Liberia",
        "code": "LR",
        "capital": "Monrovia",
        "region": "Africa",
        "currency": {
            "code": "LRD",
            "name": "Liberian dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/lbr.svg"
    },
    {
        "name": "Libya",
        "code": "LY",
        "capital": "Tripoli",
        "region": "Africa",
        "currency": {
            "code": "LYD",
            "name": "Libyan dinar",
            "symbol": "ل.د"
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/lby.svg"
    },
    {
        "name": "Liechtenstein",
        "code": "LI",
        "capital": "Vaduz",
        "region": "Europe",
        "currency": {
            "code": "CHF",
            "name": "Swiss franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "de",
            "name": "German"
        },
        "flag": "https://restcountries.eu/data/lie.svg"
    },
    {
        "name": "Lithuania",
        "code": "LT",
        "capital": "Vilnius",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "lt",
            "name": "Lithuanian"
        },
        "flag": "https://restcountries.eu/data/ltu.svg"
    },
    {
        "name": "Luxembourg",
        "code": "LU",
        "capital": "Luxembourg",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/lux.svg"
    },
    {
        "name": "Macao",
        "code": "MO",
        "capital": "",
        "region": "Asia",
        "currency": {
            "code": "MOP",
            "name": "Macanese pataca",
            "symbol": "P"
        },
        "language": {
            "code": "zh",
            "name": "Chinese"
        },
        "flag": "https://restcountries.eu/data/mac.svg"
    },
    {
        "name": "Macedonia (the former Yugoslav Republic of)",
        "code": "MK",
        "capital": "Skopje",
        "region": "Europe",
        "currency": {
            "code": "MKD",
            "name": "Macedonian denar",
            "symbol": "ден"
        },
        "language": {
            "code": "mk",
            "name": "Macedonian"
        },
        "flag": "https://restcountries.eu/data/mkd.svg"
    },
    {
        "name": "Madagascar",
        "code": "MG",
        "capital": "Antananarivo",
        "region": "Africa",
        "currency": {
            "code": "MGA",
            "name": "Malagasy ariary",
            "symbol": "Ar"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/mdg.svg"
    },
    {
        "name": "Malawi",
        "code": "MW",
        "capital": "Lilongwe",
        "region": "Africa",
        "currency": {
            "code": "MWK",
            "name": "Malawian kwacha",
            "symbol": "MK"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/mwi.svg"
    },
    {
        "name": "Malaysia",
        "code": "MY",
        "capital": "Kuala Lumpur",
        "region": "Asia",
        "currency": {
            "code": "MYR",
            "name": "Malaysian ringgit",
            "symbol": "RM"
        },
        "language": {
            "code": null,
            "name": "Malaysian"
        },
        "flag": "https://restcountries.eu/data/mys.svg"
    },
    {
        "name": "Maldives",
        "code": "MV",
        "capital": "Malé",
        "region": "Asia",
        "currency": {
            "code": "MVR",
            "name": "Maldivian rufiyaa",
            "symbol": ".ރ"
        },
        "language": {
            "code": "dv",
            "name": "Divehi"
        },
        "flag": "https://restcountries.eu/data/mdv.svg"
    },
    {
        "name": "Mali",
        "code": "ML",
        "capital": "Bamako",
        "region": "Africa",
        "currency": {
            "code": "XOF",
            "name": "West African CFA franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/mli.svg"
    },
    {
        "name": "Malta",
        "code": "MT",
        "capital": "Valletta",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "mt",
            "name": "Maltese"
        },
        "flag": "https://restcountries.eu/data/mlt.svg"
    },
    {
        "name": "Marshall Islands",
        "code": "MH",
        "capital": "Majuro",
        "region": "Oceania",
        "currency": {
            "code": "USD",
            "name": "United States dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/mhl.svg"
    },
    {
        "name": "Martinique",
        "code": "MQ",
        "capital": "Fort-de-France",
        "region": "Americas",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/mtq.svg"
    },
    {
        "name": "Mauritania",
        "code": "MR",
        "capital": "Nouakchott",
        "region": "Africa",
        "currency": {
            "code": "MRO",
            "name": "Mauritanian ouguiya",
            "symbol": "UM"
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/mrt.svg"
    },
    {
        "name": "Mauritius",
        "code": "MU",
        "capital": "Port Louis",
        "region": "Africa",
        "currency": {
            "code": "MUR",
            "name": "Mauritian rupee",
            "symbol": "₨"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/mus.svg"
    },
    {
        "name": "Mayotte",
        "code": "YT",
        "capital": "Mamoudzou",
        "region": "Africa",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/myt.svg"
    },
    {
        "name": "Mexico",
        "code": "MX",
        "capital": "Mexico City",
        "region": "North America",
        "currency": {
            "code": "MXN",
            "name": "Mexican peso",
            "symbol": "$"
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/mex.svg"
    },
    {
        "name": "Micronesia (Federated States of)",
        "code": "FM",
        "capital": "Palikir",
        "region": "Oceania",
        "currency": {
            "code": "USD",
            "name": "United States dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/fsm.svg"
    },
    {
        "name": "Moldova (Republic of)",
        "code": "MD",
        "capital": "Chișinău",
        "region": "Europe",
        "currency": {
            "code": "MDL",
            "name": "Moldovan leu",
            "symbol": "L"
        },
        "language": {
            "code": "ro",
            "name": "Romanian"
        },
        "flag": "https://restcountries.eu/data/mda.svg"
    },
    {
        "name": "Monaco",
        "code": "MC",
        "capital": "Monaco",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/mco.svg"
    },
    {
        "name": "Mongolia",
        "code": "MN",
        "capital": "Ulan Bator",
        "region": "Asia",
        "currency": {
            "code": "MNT",
            "name": "Mongolian tögrög",
            "symbol": "₮"
        },
        "language": {
            "code": "mn",
            "name": "Mongolian"
        },
        "flag": "https://restcountries.eu/data/mng.svg"
    },
    {
        "name": "Montenegro",
        "code": "ME",
        "capital": "Podgorica",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "sr",
            "name": "Serbian"
        },
        "flag": "https://restcountries.eu/data/mne.svg"
    },
    {
        "name": "Montserrat",
        "code": "MS",
        "capital": "Plymouth",
        "region": "North America",
        "currency": {
            "code": "XCD",
            "name": "East Caribbean dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/msr.svg"
    },
    {
        "name": "Morocco",
        "code": "MA",
        "capital": "Rabat",
        "region": "Africa",
        "currency": {
            "code": "MAD",
            "name": "Moroccan dirham",
            "symbol": "د.م."
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/mar.svg"
    },
    {
        "name": "Mozambique",
        "code": "MZ",
        "capital": "Maputo",
        "region": "Africa",
        "currency": {
            "code": "MZN",
            "name": "Mozambican metical",
            "symbol": "MT"
        },
        "language": {
            "code": "pt",
            "name": "Portuguese"
        },
        "flag": "https://restcountries.eu/data/moz.svg"
    },
    {
        "name": "Myanmar",
        "code": "MM",
        "capital": "Naypyidaw",
        "region": "Asia",
        "currency": {
            "code": "MMK",
            "name": "Burmese kyat",
            "symbol": "Ks"
        },
        "language": {
            "code": "my",
            "name": "Burmese"
        },
        "flag": "https://restcountries.eu/data/mmr.svg"
    },
    {
        "name": "Namibia",
        "code": "NA",
        "capital": "Windhoek",
        "region": "Africa",
        "currency": {
            "code": "NAD",
            "name": "Namibian dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/nam.svg"
    },
    {
        "name": "Nauru",
        "code": "NR",
        "capital": "Yaren",
        "region": "Oceania",
        "currency": {
            "code": "AUD",
            "name": "Australian dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/nru.svg"
    },
    {
        "name": "Nepal",
        "code": "NP",
        "capital": "Kathmandu",
        "region": "Asia",
        "currency": {
            "code": "NPR",
            "name": "Nepalese rupee",
            "symbol": "₨"
        },
        "language": {
            "code": "ne",
            "name": "Nepali"
        },
        "flag": "https://restcountries.eu/data/npl.svg"
    },
    {
        "name": "Netherlands",
        "code": "NL",
        "capital": "Amsterdam",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "nl",
            "name": "Dutch"
        },
        "flag": "https://restcountries.eu/data/nld.svg"
    },
    {
        "name": "New Caledonia",
        "code": "NC",
        "capital": "Nouméa",
        "region": "Oceania",
        "currency": {
            "code": "XPF",
            "name": "CFP franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/ncl.svg"
    },
    {
        "name": "New Zealand",
        "code": "NZ",
        "capital": "Wellington",
        "region": "Oceania",
        "currency": {
            "code": "NZD",
            "name": "New Zealand dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/nzl.svg"
    },
    {
        "name": "Nicaragua",
        "code": "NI",
        "capital": "Managua",
        "region": "North America",
        "currency": {
            "code": "NIO",
            "name": "Nicaraguan córdoba",
            "symbol": "C$"
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/nic.svg"
    },
    {
        "name": "Niger",
        "code": "NE",
        "capital": "Niamey",
        "region": "Africa",
        "currency": {
            "code": "XOF",
            "name": "West African CFA franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/ner.svg"
    },
    {
        "name": "Nigeria",
        "code": "NG",
        "capital": "Abuja",
        "region": "Africa",
        "currency": {
            "code": "NGN",
            "name": "Nigerian naira",
            "symbol": "₦"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/nga.svg"
    },
    {
        "name": "Niue",
        "code": "NU",
        "capital": "Alofi",
        "region": "Oceania",
        "currency": {
            "code": "NZD",
            "name": "New Zealand dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/niu.svg"
    },
    {
        "name": "Norfolk Island",
        "code": "NF",
        "capital": "Kingston",
        "region": "Oceania",
        "currency": {
            "code": "AUD",
            "name": "Australian dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/nfk.svg"
    },
    {
        "name": "Korea (Democratic People's Republic of)",
        "code": "KP",
        "capital": "Pyongyang",
        "region": "Asia",
        "currency": {
            "code": "KPW",
            "name": "North Korean won",
            "symbol": "₩"
        },
        "language": {
            "code": "ko",
            "name": "Korean"
        },
        "flag": "https://restcountries.eu/data/prk.svg"
    },
    {
        "name": "Northern Mariana Islands",
        "code": "MP",
        "capital": "Saipan",
        "region": "Oceania",
        "currency": {
            "code": "USD",
            "name": "United States dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/mnp.svg"
    },
    {
        "name": "Norway",
        "code": "NO",
        "capital": "Oslo",
        "region": "Europe",
        "currency": {
            "code": "NOK",
            "name": "Norwegian krone",
            "symbol": "kr"
        },
        "language": {
            "code": "no",
            "name": "Norwegian"
        },
        "flag": "https://restcountries.eu/data/nor.svg"
    },
    {
        "name": "Oman",
        "code": "OM",
        "capital": "Muscat",
        "region": "Asia",
        "currency": {
            "code": "OMR",
            "name": "Omani rial",
            "symbol": "ر.ع."
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/omn.svg"
    },
    {
        "name": "Pakistan",
        "code": "PK",
        "capital": "Islamabad",
        "region": "Asia",
        "currency": {
            "code": "PKR",
            "name": "Pakistani rupee",
            "symbol": "₨"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/pak.svg"
    },
    {
        "name": "Palau",
        "code": "PW",
        "capital": "Ngerulmud",
        "region": "Oceania",
        "currency": {
            "code": "USD",
            "name": "United States dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/plw.svg"
    },
    {
        "name": "Palestine, State of",
        "code": "PS",
        "capital": "Ramallah",
        "region": "Asia",
        "currency": {
            "code": "ILS",
            "name": "Israeli new sheqel",
            "symbol": "₪"
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/pse.svg"
    },
    {
        "name": "Panama",
        "code": "PA",
        "capital": "Panama City",
        "region": "North America",
        "currency": {
            "code": "USD",
            "name": "United States dollar",
            "symbol": "$"
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/pan.svg"
    },
    {
        "name": "Papua New Guinea",
        "code": "PG",
        "capital": "Port Moresby",
        "region": "Oceania",
        "currency": {
            "code": "PGK",
            "name": "Papua New Guinean kina",
            "symbol": "K"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/png.svg"
    },
    {
        "name": "Paraguay",
        "code": "PY",
        "capital": "Asunción",
        "region": "South America",
        "currency": {
            "code": "PYG",
            "name": "Paraguayan guaraní",
            "symbol": "₲"
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/pry.svg"
    },
    {
        "name": "Peru",
        "code": "PE",
        "capital": "Lima",
        "region": "South America",
        "currency": {
            "code": "PEN",
            "name": "Peruvian sol",
            "symbol": "S/."
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/per.svg"
    },
    {
        "name": "Philippines",
        "code": "PH",
        "capital": "Manila",
        "region": "Asia",
        "currency": {
            "code": "PHP",
            "name": "Philippine peso",
            "symbol": "₱"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/phl.svg"
    },
    {
        "name": "Pitcairn",
        "code": "PN",
        "capital": "Adamstown",
        "region": "Oceania",
        "currency": {
            "code": "NZD",
            "name": "New Zealand dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/pcn.svg"
    },
    {
        "name": "Poland",
        "code": "PL",
        "capital": "Warsaw",
        "region": "Europe",
        "currency": {
            "code": "PLN",
            "name": "Polish złoty",
            "symbol": "zł"
        },
        "language": {
            "code": "pl",
            "name": "Polish"
        },
        "flag": "https://restcountries.eu/data/pol.svg"
    },
    {
        "name": "Portugal",
        "code": "PT",
        "capital": "Lisbon",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "pt",
            "name": "Portuguese"
        },
        "flag": "https://restcountries.eu/data/prt.svg"
    },
    {
        "name": "Puerto Rico",
        "code": "PR",
        "capital": "San Juan",
        "region": "North America",
        "currency": {
            "code": "USD",
            "name": "United States dollar",
            "symbol": "$"
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/pri.svg"
    },
    {
        "name": "Qatar",
        "code": "QA",
        "capital": "Doha",
        "region": "Asia",
        "currency": {
            "code": "QAR",
            "name": "Qatari riyal",
            "symbol": "ر.ق"
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/qat.svg"
    },
    {
        "name": "Republic of Kosovo",
        "code": "XK",
        "capital": "Pristina",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "sq",
            "name": "Albanian"
        },
        "flag": "https://restcountries.eu/data/kos.svg"
    },
    {
        "name": "Réunion",
        "code": "RE",
        "capital": "Saint-Denis",
        "region": "Africa",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/reu.svg"
    },
    {
        "name": "Romania",
        "code": "RO",
        "capital": "Bucharest",
        "region": "Europe",
        "currency": {
            "code": "RON",
            "name": "Romanian leu",
            "symbol": "lei"
        },
        "language": {
            "code": "ro",
            "name": "Romanian"
        },
        "flag": "https://restcountries.eu/data/rou.svg"
    },
    {
        "name": "Russian Federation",
        "code": "RU",
        "capital": "Moscow",
        "region": "Europe",
        "currency": {
            "code": "RUB",
            "name": "Russian ruble",
            "symbol": "₽"
        },
        "language": {
            "code": "ru",
            "name": "Russian"
        },
        "flag": "https://restcountries.eu/data/rus.svg"
    },
    {
        "name": "Rwanda",
        "code": "RW",
        "capital": "Kigali",
        "region": "Africa",
        "currency": {
            "code": "RWF",
            "name": "Rwandan franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "rw",
            "name": "Kinyarwanda"
        },
        "flag": "https://restcountries.eu/data/rwa.svg"
    },
    {
        "name": "Saint Barthélemy",
        "code": "BL",
        "capital": "Gustavia",
        "region": "North America",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/blm.svg"
    },
    {
        "name": "Saint Helena, Ascension and Tristan da Cunha",
        "code": "SH",
        "capital": "Jamestown",
        "region": "Africa",
        "currency": {
            "code": "SHP",
            "name": "Saint Helena pound",
            "symbol": "£"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/shn.svg"
    },
    {
        "name": "Saint Kitts and Nevis",
        "code": "KN",
        "capital": "Basseterre",
        "region": "North America",
        "currency": {
            "code": "XCD",
            "name": "East Caribbean dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/kna.svg"
    },
    {
        "name": "Saint Lucia",
        "code": "LC",
        "capital": "Castries",
        "region": "North America",
        "currency": {
            "code": "XCD",
            "name": "East Caribbean dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/lca.svg"
    },
    {
        "name": "Saint Martin (French part)",
        "code": "MF",
        "capital": "Marigot",
        "region": "North America",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/maf.svg"
    },
    {
        "name": "Saint Pierre and Miquelon",
        "code": "PM",
        "capital": "Saint-Pierre",
        "region": "North America",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/spm.svg"
    },
    {
        "name": "Saint Vincent and the Grenadines",
        "code": "VC",
        "capital": "Kingstown",
        "region": "North America",
        "currency": {
            "code": "XCD",
            "name": "East Caribbean dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/vct.svg"
    },
    {
        "name": "Samoa",
        "code": "WS",
        "capital": "Apia",
        "region": "Oceania",
        "currency": {
            "code": "WST",
            "name": "Samoan tālā",
            "symbol": "T"
        },
        "language": {
            "code": "sm",
            "name": "Samoan"
        },
        "flag": "https://restcountries.eu/data/wsm.svg"
    },
    {
        "name": "San Marino",
        "code": "SM",
        "capital": "City of San Marino",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "it",
            "name": "Italian"
        },
        "flag": "https://restcountries.eu/data/smr.svg"
    },
    {
        "name": "Sao Tome and Principe",
        "code": "ST",
        "capital": "São Tomé",
        "region": "Africa",
        "currency": {
            "code": "STD",
            "name": "São Tomé and Príncipe dobra",
            "symbol": "Db"
        },
        "language": {
            "code": "pt",
            "name": "Portuguese"
        },
        "flag": "https://restcountries.eu/data/stp.svg"
    },
    {
        "name": "Saudi Arabia",
        "code": "SA",
        "capital": "Riyadh",
        "region": "Asia",
        "currency": {
            "code": "SAR",
            "name": "Saudi riyal",
            "symbol": "ر.س"
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/sau.svg"
    },
    {
        "name": "Senegal",
        "code": "SN",
        "capital": "Dakar",
        "region": "Africa",
        "currency": {
            "code": "XOF",
            "name": "West African CFA franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/sen.svg"
    },
    {
        "name": "Serbia",
        "code": "RS",
        "capital": "Belgrade",
        "region": "Europe",
        "currency": {
            "code": "RSD",
            "name": "Serbian dinar",
            "symbol": "дин."
        },
        "language": {
            "code": "sr",
            "name": "Serbian"
        },
        "flag": "https://restcountries.eu/data/srb.svg"
    },
    {
        "name": "Seychelles",
        "code": "SC",
        "capital": "Victoria",
        "region": "Africa",
        "currency": {
            "code": "SCR",
            "name": "Seychellois rupee",
            "symbol": "₨"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/syc.svg"
    },
    {
        "name": "Sierra Leone",
        "code": "SL",
        "capital": "Freetown",
        "region": "Africa",
        "currency": {
            "code": "SLL",
            "name": "Sierra Leonean leone",
            "symbol": "Le"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/sle.svg"
    },
    {
        "name": "Singapore",
        "code": "SG",
        "capital": "Singapore",
        "region": "Asia",
        "currency": {
            "code": "SGD",
            "name": "Singapore dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/sgp.svg"
    },
    {
        "name": "Sint Maarten (Dutch part)",
        "code": "SX",
        "capital": "Philipsburg",
        "region": "Americas",
        "currency": {
            "code": "ANG",
            "name": "Netherlands Antillean guilder",
            "symbol": "ƒ"
        },
        "language": {
            "code": "nl",
            "name": "Dutch"
        },
        "flag": "https://restcountries.eu/data/sxm.svg"
    },
    {
        "name": "Slovakia",
        "code": "SK",
        "capital": "Bratislava",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "sk",
            "name": "Slovak"
        },
        "flag": "https://restcountries.eu/data/svk.svg"
    },
    {
        "name": "Slovenia",
        "code": "SI",
        "capital": "Ljubljana",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "sl",
            "name": "Slovene"
        },
        "flag": "https://restcountries.eu/data/svn.svg"
    },
    {
        "name": "Solomon Islands",
        "code": "SB",
        "capital": "Honiara",
        "region": "Oceania",
        "currency": {
            "code": "SBD",
            "name": "Solomon Islands dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/slb.svg"
    },
    {
        "name": "Somalia",
        "code": "SO",
        "capital": "Mogadishu",
        "region": "Africa",
        "currency": {
            "code": "SOS",
            "name": "Somali shilling",
            "symbol": "Sh"
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/som.svg"
    },
    {
        "name": "South Africa",
        "code": "ZA",
        "capital": "Pretoria",
        "region": "Africa",
        "currency": {
            "code": "ZAR",
            "name": "South African rand",
            "symbol": "R"
        },
        "language": {
            "code": "en",
            "iso639_2": "eng",
            "name": "English",
            "nativeName": "English"
        },
        "flag": "https://restcountries.eu/data/zaf.svg"
    },
    {
        "name": "South Georgia and the South Sandwich Islands",
        "code": "GS",
        "capital": "King Edward Point",
        "region": "North America",
        "currency": {
            "code": "GBP",
            "name": "British pound",
            "symbol": "£"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/sgs.svg"
    },
    {
        "name": "Korea (Republic of)",
        "code": "KR",
        "capital": "Seoul",
        "region": "Asia",
        "currency": {
            "code": "KRW",
            "name": "South Korean won",
            "symbol": "₩"
        },
        "language": {
            "code": "ko",
            "name": "Korean"
        },
        "flag": "https://restcountries.eu/data/kor.svg"
    },
    {
        "name": "South Sudan",
        "code": "SS",
        "capital": "Juba",
        "region": "Africa",
        "currency": {
            "code": "SSP",
            "name": "South Sudanese pound",
            "symbol": "£"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/ssd.svg"
    },
    {
        "name": "Spain",
        "code": "ES",
        "capital": "Madrid",
        "region": "Europe",
        "currency": {
            "code": "EUR",
            "name": "Euro",
            "symbol": "€"
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/esp.svg"
    },
    {
        "name": "Sri Lanka",
        "code": "LK",
        "capital": "Colombo",
        "region": "Asia",
        "currency": {
            "code": "LKR",
            "name": "Sri Lankan rupee",
            "symbol": "Rs"
        },
        "language": {
            "code": "si",
            "iso639_2": "sin",
            "name": "Sinhalese",
            "nativeName": "සිංහල"
        },
        "flag": "https://restcountries.eu/data/lka.svg"
    },
    {
        "name": "Sudan",
        "code": "SD",
        "capital": "Khartoum",
        "region": "Africa",
        "currency": {
            "code": "SDG",
            "name": "Sudanese pound",
            "symbol": "ج.س."
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/sdn.svg"
    },
    {
        "name": "Suriname",
        "code": "SR",
        "capital": "Paramaribo",
        "region": "South America",
        "currency": {
            "code": "SRD",
            "name": "Surinamese dollar",
            "symbol": "$"
        },
        "language": {
            "code": "nl",
            "name": "Dutch"
        },
        "flag": "https://restcountries.eu/data/sur.svg"
    },
    {
        "name": "Svalbard and Jan Mayen",
        "code": "SJ",
        "capital": "Longyearbyen",
        "region": "Europe",
        "currency": {
            "code": "NOK",
            "name": "Norwegian krone",
            "symbol": "kr"
        },
        "language": {
            "code": "no",
            "name": "Norwegian"
        },
        "flag": "https://restcountries.eu/data/sjm.svg"
    },
    {
        "name": "Swaziland",
        "code": "SZ",
        "capital": "Lobamba",
        "region": "Africa",
        "currency": {
            "code": "SZL",
            "name": "Swazi lilangeni",
            "symbol": "L"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/swz.svg"
    },
    {
        "name": "Sweden",
        "code": "SE",
        "capital": "Stockholm",
        "region": "Europe",
        "currency": {
            "code": "SEK",
            "name": "Swedish krona",
            "symbol": "kr"
        },
        "language": {
            "code": "sv",
            "name": "Swedish"
        },
        "flag": "https://restcountries.eu/data/swe.svg"
    },
    {
        "name": "Switzerland",
        "code": "CH",
        "capital": "Bern",
        "region": "Europe",
        "currency": {
            "code": "CHF",
            "name": "Swiss franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "de",
            "name": "German"
        },
        "flag": "https://restcountries.eu/data/che.svg"
    },
    {
        "name": "Syrian Arab Republic",
        "code": "SY",
        "capital": "Damascus",
        "region": "Asia",
        "currency": {
            "code": "SYP",
            "name": "Syrian pound",
            "symbol": "£"
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/syr.svg"
    },
    {
        "name": "Taiwan",
        "code": "TW",
        "capital": "Taipei",
        "region": "Asia",
        "currency": {
            "code": "TWD",
            "name": "New Taiwan dollar",
            "symbol": "$"
        },
        "language": {
            "code": "zh",
            "name": "Chinese"
        },
        "flag": "https://restcountries.eu/data/twn.svg"
    },
    {
        "name": "Tajikistan",
        "code": "TJ",
        "capital": "Dushanbe",
        "region": "Asia",
        "currency": {
            "code": "TJS",
            "name": "Tajikistani somoni",
            "symbol": "ЅМ"
        },
        "language": {
            "code": "tg",
            "name": "Tajik"
        },
        "flag": "https://restcountries.eu/data/tjk.svg"
    },
    {
        "name": "Tanzania, United Republic of",
        "code": "TZ",
        "capital": "Dodoma",
        "region": "Africa",
        "currency": {
            "code": "TZS",
            "name": "Tanzanian shilling",
            "symbol": "Sh"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/tza.svg"
    },
    {
        "name": "Thailand",
        "code": "TH",
        "capital": "Bangkok",
        "region": "Asia",
        "currency": {
            "code": "THB",
            "name": "Thai baht",
            "symbol": "฿"
        },
        "language": {
            "code": "th",
            "name": "Thai"
        },
        "flag": "https://restcountries.eu/data/tha.svg"
    },
    {
        "name": "Timor-Leste",
        "code": "TL",
        "capital": "Dili",
        "region": "Asia",
        "currency": {
            "code": "USD",
            "name": "United States dollar",
            "symbol": "$"
        },
        "language": {
            "code": "pt",
            "name": "Portuguese"
        },
        "flag": "https://restcountries.eu/data/tls.svg"
    },
    {
        "name": "Togo",
        "code": "TG",
        "capital": "Lomé",
        "region": "Africa",
        "currency": {
            "code": "XOF",
            "name": "West African CFA franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/tgo.svg"
    },
    {
        "name": "Tokelau",
        "code": "TK",
        "capital": "Fakaofo",
        "region": "Oceania",
        "currency": {
            "code": "NZD",
            "name": "New Zealand dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/tkl.svg"
    },
    {
        "name": "Tonga",
        "code": "TO",
        "capital": "Nuku'alofa",
        "region": "Oceania",
        "currency": {
            "code": "TOP",
            "name": "Tongan paʻanga",
            "symbol": "T$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/ton.svg"
    },
    {
        "name": "Trinidad and Tobago",
        "code": "TT",
        "capital": "Port of Spain",
        "region": "South America",
        "currency": {
            "code": "TTD",
            "name": "Trinidad and Tobago dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/tto.svg"
    },
    {
        "name": "Tunisia",
        "code": "TN",
        "capital": "Tunis",
        "region": "Africa",
        "currency": {
            "code": "TND",
            "name": "Tunisian dinar",
            "symbol": "د.ت"
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/tun.svg"
    },
    {
        "name": "Turkey",
        "code": "TR",
        "capital": "Ankara",
        "region": "Asia",
        "currency": {
            "code": "TRY",
            "name": "Turkish lira",
            "symbol": null
        },
        "language": {
            "code": "tr",
            "name": "Turkish"
        },
        "flag": "https://restcountries.eu/data/tur.svg"
    },
    {
        "name": "Turkmenistan",
        "code": "TM",
        "capital": "Ashgabat",
        "region": "Asia",
        "currency": {
            "code": "TMT",
            "name": "Turkmenistan manat",
            "symbol": "m"
        },
        "language": {
            "code": "tk",
            "name": "Turkmen"
        },
        "flag": "https://restcountries.eu/data/tkm.svg"
    },
    {
        "name": "Turks and Caicos Islands",
        "code": "TC",
        "capital": "Cockburn Town",
        "region": "North America",
        "currency": {
            "code": "USD",
            "name": "United States dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/tca.svg"
    },
    {
        "name": "Tuvalu",
        "code": "TV",
        "capital": "Funafuti",
        "region": "Oceania",
        "currency": {
            "code": "AUD",
            "name": "Australian dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/tuv.svg"
    },
    {
        "name": "Uganda",
        "code": "UG",
        "capital": "Kampala",
        "region": "Africa",
        "currency": {
            "code": "UGX",
            "name": "Ugandan shilling",
            "symbol": "Sh"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/uga.svg"
    },
    {
        "name": "Ukraine",
        "code": "UA",
        "capital": "Kiev",
        "region": "Europe",
        "currency": {
            "code": "UAH",
            "name": "Ukrainian hryvnia",
            "symbol": "₴"
        },
        "language": {
            "code": "uk",
            "name": "Ukrainian"
        },
        "flag": "https://restcountries.eu/data/ukr.svg"
    },
    {
        "name": "United Arab Emirates",
        "code": "AE",
        "capital": "Abu Dhabi",
        "region": "Asia",
        "currency": {
            "code": "AED",
            "name": "United Arab Emirates dirham",
            "symbol": "د.إ"
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/are.svg"
    },
    {
        "name": "United Kingdom of Great Britain and Northern Ireland",
        "code": "GB",
        "capital": "London",
        "region": "Europe",
        "currency": {
            "code": "GBP",
            "name": "British pound",
            "symbol": "£"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/gbr.svg"
    },
    {
        "name": "United States of America",
        "code": "US",
        "capital": "Washington, D.C.",
        "region": "North America",
        "currency": {
            "code": "USD",
            "name": "United States dollar",
            "symbol": "$"
        },
        "language": {
            "code": "en",
            "iso639_2": "eng",
            "name": "English",
            "nativeName": "English"
        },
        "flag": "https://restcountries.eu/data/usa.svg"
    },
    {
        "name": "Uruguay",
        "code": "UY",
        "capital": "Montevideo",
        "region": "South America",
        "currency": {
            "code": "UYU",
            "name": "Uruguayan peso",
            "symbol": "$"
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/ury.svg"
    },
    {
        "name": "Uzbekistan",
        "code": "UZ",
        "capital": "Tashkent",
        "region": "Asia",
        "currency": {
            "code": "UZS",
            "name": "Uzbekistani so'm",
            "symbol": null
        },
        "language": {
            "code": "uz",
            "name": "Uzbek"
        },
        "flag": "https://restcountries.eu/data/uzb.svg"
    },
    {
        "name": "Vanuatu",
        "code": "VU",
        "capital": "Port Vila",
        "region": "Oceania",
        "currency": {
            "code": "VUV",
            "name": "Vanuatu vatu",
            "symbol": "Vt"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/vut.svg"
    },
    {
        "name": "Venezuela (Bolivarian Republic of)",
        "code": "VE",
        "capital": "Caracas",
        "region": "South America",
        "currency": {
            "code": "VEF",
            "name": "Venezuelan bolívar",
            "symbol": "Bs F"
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/ven.svg"
    },
    {
        "name": "Viet Nam",
        "code": "VN",
        "capital": "Hanoi",
        "region": "Asia",
        "currency": {
            "code": "VND",
            "name": "Vietnamese đồng",
            "symbol": "₫"
        },
        "language": {
            "code": "vi",
            "name": "Vietnamese"
        },
        "flag": "https://restcountries.eu/data/vnm.svg"
    },
    {
        "name": "Wallis and Futuna",
        "code": "WF",
        "capital": "Mata-Utu",
        "region": "Oceania",
        "currency": {
            "code": "XPF",
            "name": "CFP franc",
            "symbol": "Fr"
        },
        "language": {
            "code": "fr",
            "name": "French"
        },
        "flag": "https://restcountries.eu/data/wlf.svg"
    },
    {
        "name": "Western Sahara",
        "code": "EH",
        "capital": "El Aaiún",
        "region": "Africa",
        "currency": {
            "code": "MAD",
            "name": "Moroccan dirham",
            "symbol": "د.م."
        },
        "language": {
            "code": "es",
            "name": "Spanish"
        },
        "flag": "https://restcountries.eu/data/esh.svg"
    },
    {
        "name": "Yemen",
        "code": "YE",
        "capital": "Sana'a",
        "region": "Asia",
        "currency": {
            "code": "YER",
            "name": "Yemeni rial",
            "symbol": "﷼"
        },
        "language": {
            "code": "ar",
            "name": "Arabic"
        },
        "flag": "https://restcountries.eu/data/yem.svg"
    },
    {
        "name": "Zambia",
        "code": "ZM",
        "capital": "Lusaka",
        "region": "Africa",
        "currency": {
            "code": "ZMW",
            "name": "Zambian kwacha",
            "symbol": "ZK"
        },
        "language": {
            "code": "en",
            "name": "English"
        },
        "flag": "https://restcountries.eu/data/zmb.svg"
    },
    {
        "name": "Zimbabwe",
        "code": "ZW",
        "capital": "Harare",
        "region": "Africa",
        "currency": {
            "code": "BWP",
            "name": "Botswana pula",
            "symbol": "P"
        },
        "language": {
            "code": "en",
            "iso639_2": "eng",
            "name": "English",
            "nativeName": "English"
        },
        "flag": "https://restcountries.eu/data/zwe.svg"
    }
]

export const ciso = [{
    Name: 'Afghanistan',
    Code: 'AF',
    Timezone: 'Afghanistan Standard Time',
    UTC: 'UTC+04:30',
    MobileCode: '+93'
}, {
    Name: 'Åland Islands',
    Code: 'AX',
    Timezone: 'FLE Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+358-18'
}, {
    Name: 'Albania',
    Code: 'AL',
    Timezone: 'Central Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+355'
}, {
    Name: 'Algeria',
    Code: 'DZ',
    Timezone: 'W. Central Africa Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+213'
}, {
    Name: 'American Samoa',
    Code: 'AS',
    Timezone: 'UTC-11',
    UTC: 'UTC-11:00',
    MobileCode: '+1-684'
}, {
    Name: 'Andorra',
    Code: 'AD',
    Timezone: 'W. Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+376'
}, {
    Name: 'Angola',
    Code: 'AO',
    Timezone: 'W. Central Africa Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+244'
}, {
    Name: 'Anguilla',
    Code: 'AI',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+1-264'
},{
    Name: 'Argentina',
    Code: 'AR',
    Timezone: 'Argentina Standard Time',
    UTC: 'UTC-03:00',
    MobileCode: '+54'
}, {
    Name: 'Armenia',
    Code: 'AM',
    Timezone: 'Caucasus Standard Time',
    UTC: 'UTC+04:00',
    MobileCode: '+374'
}, {
    Name: 'Aruba',
    Code: 'AW',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+297'
}, {
    Name: 'Australia',
    Code: 'AU',
    Timezone: 'AUS Eastern Standard Time',
    UTC: 'UTC+10:00',
    MobileCode: '+61'
}, {
    Name: 'Austria',
    Code: 'AT',
    Timezone: 'W. Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+43'
}, {
    Name: 'Azerbaijan',
    Code: 'AZ',
    Timezone: 'Azerbaijan Standard Time',
    UTC: 'UTC+04:00',
    MobileCode: '+994'
}, {
    Name: 'Bahamas, The',
    Code: 'BS',
    Timezone: 'Eastern Standard Time',
    UTC: 'UTC-05:00',
    MobileCode: '+1-242'
}, {
    Name: 'Bahrain',
    Code: 'BH',
    Timezone: 'Arab Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+973'
}, {
    Name: 'Bangladesh',
    Code: 'BD',
    Timezone: 'Bangladesh Standard Time',
    UTC: 'UTC+06:00',
    MobileCode: '+880'
}, {
    Name: 'Barbados',
    Code: 'BB',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+1-246'
}, {
    Name: 'Belarus',
    Code: 'BY',
    Timezone: 'Belarus Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+375'
}, {
    Name: 'Belgium',
    Code: 'BE',
    Timezone: 'Romance Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+32'
}, {
    Name: 'Belize',
    Code: 'BZ',
    Timezone: 'Central America Standard Time',
    UTC: 'UTC-06:00',
    MobileCode: '+501'
}, {
    Name: 'Benin',
    Code: 'BJ',
    Timezone: 'W. Central Africa Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+229'
}, {
    Name: 'Bermuda',
    Code: 'BM',
    Timezone: 'Atlantic Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+1-441'
}, {
    Name: 'Bhutan',
    Code: 'BT',
    Timezone: 'Bangladesh Standard Time',
    UTC: 'UTC+06:00',
    MobileCode: '+975'
}, {
    Name: 'Bolivia',
    Code: 'BO',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+591'
}, {
    Name: 'Bonaire, Sint Eustatius and Saba',
    Code: 'BQ',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+599'
}, {
    Name: 'Bosnia and Herzegovina',
    Code: 'BA',
    Timezone: 'Central European Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+387'
}, {
    Name: 'Botswana',
    Code: 'BW',
    Timezone: 'South Africa Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+267'
}, {
    Name: 'Brazil',
    Code: 'BR',
    Timezone: 'E. South America Standard Time',
    UTC: 'UTC-03:00',
    MobileCode: '+55'
}, {
    Name: 'British Indian Ocean Territory',
    Code: 'IO',
    Timezone: 'Central Asia Standard Time',
    UTC: 'UTC+06:00',
    MobileCode: '+246'
}, {
    Name: 'Brunei',
    Code: 'BN',
    Timezone: 'Singapore Standard Time',
    UTC: 'UTC+08:00',
    MobileCode: '+673'
}, {
    Name: 'Bulgaria',
    Code: 'BG',
    Timezone: 'FLE Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+359'
}, {
    Name: 'Burkina Faso',
    Code: 'BF',
    Timezone: 'Greenwich Standard Time',
    UTC: 'UTC',
    MobileCode: '+226'
}, {
    Name: 'Burundi',
    Code: 'BI',
    Timezone: 'South Africa Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+257'
}, {
    Name: 'Cabo Verde',
    Code: 'CV',
    Timezone: 'Cape Verde Standard Time',
    UTC: 'UTC-01:00',
    MobileCode: '+238'
}, {
    Name: 'Cambodia',
    Code: 'KH',
    Timezone: 'SE Asia Standard Time',
    UTC: 'UTC+07:00',
    MobileCode: '+855'
}, {
    Name: 'Cameroon',
    Code: 'CM',
    Timezone: 'W. Central Africa Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+237'
}, {
    Name: 'Canada',
    Code: 'CA',
    Timezone: 'Eastern Standard Time',
    UTC: 'UTC-05:00',
    MobileCode: '+1'
}, {
    Name: 'Cayman Islands',
    Code: 'KY',
    Timezone: 'SA Pacific Standard Time',
    UTC: 'UTC-05:00',
    MobileCode: '+1-345'
}, {
    Name: 'Central African Republic',
    Code: 'CF',
    Timezone: 'W. Central Africa Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+236'
}, {
    Name: 'Chad',
    Code: 'TD',
    Timezone: 'W. Central Africa Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+235'
}, {
    Name: 'Chile',
    Code: 'CL',
    Timezone: 'Pacific SA Standard Time',
    UTC: 'UTC-03:00',
    MobileCode: '+56'
}, {
    Name: 'China',
    Code: 'CN',
    Timezone: 'China Standard Time',
    UTC: 'UTC+08:00',
    MobileCode: '+86'
}, {
    Name: 'Colombia',
    Code: 'CO',
    Timezone: 'SA Pacific Standard Time',
    UTC: 'UTC-05:00',
    MobileCode: '+57'
}, {
    Name: 'Comoros',
    Code: 'KM',
    Timezone: 'E. Africa Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+269'
}, {
    Name: 'Congo',
    Code: 'CG',
    Timezone: 'W. Central Africa Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+242'
}, {
    Name: 'Congo (DRC)',
    Code: 'CD',
    Timezone: 'W. Central Africa Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+243'
}, {
    Name: 'Cook Islands',
    Code: 'CK',
    Timezone: 'Hawaiian Standard Time',
    UTC: 'UTC-10:00',
    MobileCode: '+682'
}, {
    Name: 'Costa Rica',
    Code: 'CR',
    Timezone: 'Central America Standard Time',
    UTC: 'UTC-06:00',
    MobileCode: '+506'
}, {
    Name: "Côte d'Ivoire",
    Code: 'CI',
    Timezone: 'Greenwich Standard Time',
    UTC: 'UTC',
    MobileCode: '+225'
}, {
    Name: 'Croatia',
    Code: 'HR',
    Timezone: 'Central European Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+385'
}, {
    Name: 'Cuba',
    Code: 'CU',
    Timezone: 'Eastern Standard Time',
    UTC: 'UTC-05:00',
    MobileCode: '+53'
}, {
    Name: 'Curaçao',
    Code: 'CW',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+599'
}, {
    Name: 'Cyprus',
    Code: 'CY',
    Timezone: 'E. Europe Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+357'
}, {
    Name: 'Czech Republic',
    Code: 'CZ',
    Timezone: 'Central Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+420'
}, {
    Name: 'Democratic Republic of Timor-Leste',
    Code: 'TL',
    Timezone: 'Tokyo Standard Time',
    UTC: 'UTC+09:00',
    MobileCode: '+670'
}, {
    Name: 'Denmark',
    Code: 'DK',
    Timezone: 'Romance Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+45'
}, {
    Name: 'Djibouti',
    Code: 'DJ',
    Timezone: 'E. Africa Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+253'
}, {
    Name: 'Dominica',
    Code: 'DM',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+1-767'
}, {
    Name: 'Dominican Republic',
    Code: 'DO',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+1-809 and 1-829'
}, {
    Name: 'Ecuador',
    Code: 'EC',
    Timezone: 'SA Pacific Standard Time',
    UTC: 'UTC-05:00',
    MobileCode: '+593'
}, {
    Name: 'Egypt',
    Code: 'EG',
    Timezone: 'Egypt Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+20'
}, {
    Name: 'El Salvador',
    Code: 'SV',
    Timezone: 'Central America Standard Time',
    UTC: 'UTC-06:00',
    MobileCode: '+503'
}, {
    Name: 'Equatorial Guinea',
    Code: 'GQ',
    Timezone: 'W. Central Africa Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+240'
}, {
    Name: 'Eritrea',
    Code: 'ER',
    Timezone: 'E. Africa Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+291'
}, {
    Name: 'Estonia',
    Code: 'EE',
    Timezone: 'FLE Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+372'
}, {
    Name: 'Ethiopia',
    Code: 'ET',
    Timezone: 'E. Africa Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+251'
}, {
    Name: 'Falkland Islands (Islas Malvinas)',
    Code: 'FK',
    Timezone: 'SA Eastern Standard Time',
    UTC: 'UTC-03:00',
    MobileCode: '+500'
}, {
    Name: 'Faroe Islands',
    Code: 'FO',
    Timezone: 'GMT Standard Time',
    UTC: 'UTC',
    MobileCode: '+298'
}, {
    Name: 'Fiji Islands',
    Code: 'FJ',
    Timezone: 'Fiji Standard Time',
    UTC: 'UTC+12:00',
    MobileCode: '+679'
}, {
    Name: 'Finland',
    Code: 'FI',
    Timezone: 'FLE Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+358'
}, {
    Name: 'France',
    Code: 'FR',
    Timezone: 'Romance Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+33'
}, {
    Name: 'French Guiana',
    Code: 'GF',
    Timezone: 'SA Eastern Standard Time',
    UTC: 'UTC-03:00',
    MobileCode: '+594'
}, {
    Name: 'French Polynesia',
    Code: 'PF',
    Timezone: 'Hawaiian Standard Time',
    UTC: 'UTC-10:00',
    MobileCode: '+689'
}, {
    Name: 'Gabon',
    Code: 'GA',
    Timezone: 'W. Central Africa Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+241'
}, {
    Name: 'Gambia, The',
    Code: 'GM',
    Timezone: 'Greenwich Standard Time',
    UTC: 'UTC',
    MobileCode: '+220'
}, {
    Name: 'Georgia',
    Code: 'GE',
    Timezone: 'Georgian Standard Time',
    UTC: 'UTC+04:00',
    MobileCode: '+995'
}, {
    Name: 'Germany',
    Code: 'DE',
    Timezone: 'W. Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+49'
}, {
    Name: 'Ghana',
    Code: 'GH',
    Timezone: 'Greenwich Standard Time',
    UTC: 'UTC',
    MobileCode: '+233'
}, {
    Name: 'Gibraltar',
    Code: 'GI',
    Timezone: 'W. Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+350'
}, {
    Name: 'Greece',
    Code: 'GR',
    Timezone: 'GTB Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+30'
}, {
    Name: 'Greenland',
    Code: 'GL',
    Timezone: 'Greenland Standard Time',
    UTC: 'UTC-03:00',
    MobileCode: '+299'
}, {
    Name: 'Grenada',
    Code: 'GD',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+1-473'
}, {
    Name: 'Guadeloupe',
    Code: 'GP',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+590'
}, {
    Name: 'Guam',
    Code: 'GU',
    Timezone: 'West Pacific Standard Time',
    UTC: 'UTC+10:00',
    MobileCode: '+1-671'
}, {
    Name: 'Guatemala',
    Code: 'GT',
    Timezone: 'Central America Standard Time',
    UTC: 'UTC-06:00',
    MobileCode: '+502'
}, {
    Name: 'Guernsey',
    Code: 'GG',
    Timezone: 'GMT Standard Time',
    UTC: 'UTC',
    MobileCode: '+44-1481'
}, {
    Name: 'Guinea',
    Code: 'GN',
    Timezone: 'Greenwich Standard Time',
    UTC: 'UTC',
    MobileCode: '+224'
}, {
    Name: 'Guinea-Bissau',
    Code: 'GW',
    Timezone: 'Greenwich Standard Time',
    UTC: 'UTC',
    MobileCode: '+245'
}, {
    Name: 'Guyana',
    Code: 'GY',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+592'
}, {
    Name: 'Haiti',
    Code: 'HT',
    Timezone: 'Eastern Standard Time',
    UTC: 'UTC-05:00',
    MobileCode: '+509'
}, {
    Name: 'Heard Island and McDonald Islands',
    Code: 'HM',
    Timezone: 'Mauritius Standard Time',
    UTC: 'UTC+04:00',
    MobileCode: '+ '
}, {
    Name: 'Honduras',
    Code: 'HN',
    Timezone: 'Central America Standard Time',
    UTC: 'UTC-06:00',
    MobileCode: '+504'
}, {
    Name: 'Hong Kong SAR',
    Code: 'HK',
    Timezone: 'China Standard Time',
    UTC: 'UTC+08:00',
    MobileCode: '+852'
}, {
    Name: 'Hungary',
    Code: 'HU',
    Timezone: 'Central Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+36'
}, {
    Name: 'Iceland',
    Code: 'IS',
    Timezone: 'Greenwich Standard Time',
    UTC: 'UTC',
    MobileCode: '+354'
}, {
    Name: 'India',
    Code: 'IN',
    Timezone: 'India Standard Time',
    UTC: 'UTC+05:30',
    MobileCode: '+91'
}, {
    Name: 'Indonesia',
    Code: 'ID',
    Timezone: 'SE Asia Standard Time',
    UTC: 'UTC+07:00',
    MobileCode: '+62'
}, {
    Name: 'Iran',
    Code: 'IR',
    Timezone: 'Iran Standard Time',
    UTC: 'UTC+03:30',
    MobileCode: '+98'
}, {
    Name: 'Iraq',
    Code: 'IQ',
    Timezone: 'Arabic Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+964'
}, {
    Name: 'Ireland',
    Code: 'IE',
    Timezone: 'GMT Standard Time',
    UTC: 'UTC',
    MobileCode: '+353'
}, {
    Name: 'Israel',
    Code: 'IL',
    Timezone: 'Israel Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+972'
}, {
    Name: 'Italy',
    Code: 'IT',
    Timezone: 'W. Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+39'
}, {
    Name: 'Jamaica',
    Code: 'JM',
    Timezone: 'SA Pacific Standard Time',
    UTC: 'UTC-05:00',
    MobileCode: '+1-876'
}, {
    Name: 'Jan Mayen',
    Code: 'SJ',
    Timezone: 'W. Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+47'
}, {
    Name: 'Japan',
    Code: 'JP',
    Timezone: 'Tokyo Standard Time',
    UTC: 'UTC+09:00',
    MobileCode: '+81'
}, {
    Name: 'Jersey',
    Code: 'JE',
    Timezone: 'GMT Standard Time',
    UTC: 'UTC',
    MobileCode: '+44-1534'
}, {
    Name: 'Jordan',
    Code: 'JO',
    Timezone: 'Jordan Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+962'
}, {
    Name: 'Kazakhstan',
    Code: 'KZ',
    Timezone: 'Central Asia Standard Time',
    UTC: 'UTC+06:00',
    MobileCode: '+7'
}, {
    Name: 'Kenya',
    Code: 'KE',
    Timezone: 'E. Africa Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+254'
}, {
    Name: 'Kiribati',
    Code: 'KI',
    Timezone: 'UTC+12',
    UTC: 'UTC+12:00',
    MobileCode: '+686'
}, {
    Name: 'Korea',
    Code: 'KR',
    Timezone: 'Korea Standard Time',
    UTC: 'UTC+09:00',
    MobileCode: '+82'
}, {
    Name: 'Kuwait',
    Code: 'KW',
    Timezone: 'Arab Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+965'
}, {
    Name: 'Kyrgyzstan',
    Code: 'KG',
    Timezone: 'Central Asia Standard Time',
    UTC: 'UTC+06:00',
    MobileCode: '+996'
}, {
    Name: 'Laos',
    Code: 'LA',
    Timezone: 'SE Asia Standard Time',
    UTC: 'UTC+07:00',
    MobileCode: '+856'
}, {
    Name: 'Latvia',
    Code: 'LV',
    Timezone: 'FLE Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+371'
}, {
    Name: 'Lebanon',
    Code: 'LB',
    Timezone: 'Middle East Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+961'
}, {
    Name: 'Lesotho',
    Code: 'LS',
    Timezone: 'South Africa Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+266'
}, {
    Name: 'Liberia',
    Code: 'LR',
    Timezone: 'Greenwich Standard Time',
    UTC: 'UTC',
    MobileCode: '+231'
}, {
    Name: 'Libya',
    Code: 'LY',
    Timezone: 'E. Europe Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+218'
}, {
    Name: 'Liechtenstein',
    Code: 'LI',
    Timezone: 'W. Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+423'
}, {
    Name: 'Lithuania',
    Code: 'LT',
    Timezone: 'FLE Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+370'
}, {
    Name: 'Luxembourg',
    Code: 'LU',
    Timezone: 'W. Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+352'
}, {
    Name: 'Macao SAR',
    Code: 'MO',
    Timezone: 'China Standard Time',
    UTC: 'UTC+08:00',
    MobileCode: '+853'
}, {
    Name: 'Macedonia, Former Yugoslav Republic of',
    Code: 'MK',
    Timezone: 'Central European Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+389'
}, {
    Name: 'Madagascar',
    Code: 'MG',
    Timezone: 'E. Africa Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+261'
}, {
    Name: 'Malawi',
    Code: 'MW',
    Timezone: 'South Africa Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+265'
}, {
    Name: 'Malaysia',
    Code: 'MY',
    Timezone: 'Singapore Standard Time',
    UTC: 'UTC+08:00',
    MobileCode: '+60'
}, {
    Name: 'Maldives',
    Code: 'MV',
    Timezone: 'West Asia Standard Time',
    UTC: 'UTC+05:00',
    MobileCode: '+960'
}, {
    Name: 'Mali',
    Code: 'ML',
    Timezone: 'Greenwich Standard Time',
    UTC: 'UTC',
    MobileCode: '+223'
}, {
    Name: 'Malta',
    Code: 'MT',
    Timezone: 'W. Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+356'
}, {
    Name: 'Man, Isle of',
    Code: 'IM',
    Timezone: 'GMT Standard Time',
    UTC: 'UTC',
    MobileCode: '+44-1624'
}, {
    Name: 'Marshall Islands',
    Code: 'MH',
    Timezone: 'UTC+12',
    UTC: 'UTC+12:00',
    MobileCode: '+692'
}, {
    Name: 'Martinique',
    Code: 'MQ',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+596'
}, {
    Name: 'Mauritania',
    Code: 'MR',
    Timezone: 'Greenwich Standard Time',
    UTC: 'UTC',
    MobileCode: '+222'
}, {
    Name: 'Mauritius',
    Code: 'MU',
    Timezone: 'Mauritius Standard Time',
    UTC: 'UTC+04:00',
    MobileCode: '+230'
}, {
    Name: 'Mayotte',
    Code: 'YT',
    Timezone: 'E. Africa Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+262'
}, {
    Name: 'Mexico',
    Code: 'MX',
    Timezone: 'Central Standard Time (Mexico)',
    UTC: 'UTC-06:00',
    MobileCode: '+52'
}, {
    Name: 'Micronesia',
    Code: 'FM',
    Timezone: 'West Pacific Standard Time',
    UTC: 'UTC+10:00',
    MobileCode: '+691'
}, {
    Name: 'Moldova',
    Code: 'MD',
    Timezone: 'GTB Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+373'
}, {
    Name: 'Monaco',
    Code: 'MC',
    Timezone: 'W. Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+377'
}, {
    Name: 'Mongolia',
    Code: 'MN',
    Timezone: 'Ulaanbaatar Standard Time',
    UTC: 'UTC+08:00',
    MobileCode: '+976'
}, {
    Name: 'Montenegro',
    Code: 'ME',
    Timezone: 'Central European Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+382'
}, {
    Name: 'Montserrat',
    Code: 'MS',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+1-664'
}, {
    Name: 'Morocco',
    Code: 'MA',
    Timezone: 'Morocco Standard Time',
    UTC: 'UTC',
    MobileCode: '+212'
}, {
    Name: 'Mozambique',
    Code: 'MZ',
    Timezone: 'South Africa Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+258'
}, {
    Name: 'Myanmar',
    Code: 'MM',
    Timezone: 'Myanmar Standard Time',
    UTC: 'UTC+06:30',
    MobileCode: '+95'
}, {
    Name: 'Namibia',
    Code: 'NA',
    Timezone: 'Namibia Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+264'
}, {
    Name: 'Nauru',
    Code: 'NR',
    Timezone: 'UTC+12',
    UTC: 'UTC+12:00',
    MobileCode: '+674'
}, {
    Name: 'Nepal',
    Code: 'NP',
    Timezone: 'Nepal Standard Time',
    UTC: 'UTC+05:45',
    MobileCode: '+977'
}, {
    Name: 'Netherlands',
    Code: 'NL',
    Timezone: 'W. Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+31'
}, {
    Name: 'New Caledonia',
    Code: 'NC',
    Timezone: 'Central Pacific Standard Time',
    UTC: 'UTC+11:00',
    MobileCode: '+687'
}, {
    Name: 'New Zealand',
    Code: 'NZ',
    Timezone: 'New Zealand Standard Time',
    UTC: 'UTC+12:00',
    MobileCode: '+64'
}, {
    Name: 'Nicaragua',
    Code: 'NI',
    Timezone: 'Central America Standard Time',
    UTC: 'UTC-06:00',
    MobileCode: '+505'
}, {
    Name: 'Niger',
    Code: 'NE',
    Timezone: 'W. Central Africa Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+227'
}, {
    Name: 'Nigeria',
    Code: 'NG',
    Timezone: 'W. Central Africa Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+234'
}, {
    Name: 'Niue',
    Code: 'NU',
    Timezone: 'UTC-11',
    UTC: 'UTC-11:00',
    MobileCode: '+683'
}, {
    Name: 'Norfolk Island',
    Code: 'NF',
    Timezone: 'Central Pacific Standard Time',
    UTC: 'UTC+11:00',
    MobileCode: '+672'
}, {
    Name: 'North Korea',
    Code: 'KP',
    Timezone: 'Korea Standard Time',
    UTC: 'UTC+09:00',
    MobileCode: '+850'
}, {
    Name: 'Northern Mariana Islands',
    Code: 'MP',
    Timezone: 'West Pacific Standard Time',
    UTC: 'UTC+10:00',
    MobileCode: '+1-670'
}, {
    Name: 'Norway',
    Code: 'NO',
    Timezone: 'W. Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+47'
}, {
    Name: 'Oman',
    Code: 'OM',
    Timezone: 'Arabian Standard Time',
    UTC: 'UTC+04:00',
    MobileCode: '+968'
}, {
    Name: 'Pakistan',
    Code: 'PK',
    Timezone: 'Pakistan Standard Time',
    UTC: 'UTC+05:00',
    MobileCode: '+92'
}, {
    Name: 'Palau',
    Code: 'PW',
    Timezone: 'Tokyo Standard Time',
    UTC: 'UTC+09:00',
    MobileCode: '+680'
}, {
    Name: 'Palestinian Authority',
    Code: 'PS',
    Timezone: 'Egypt Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+970'
}, {
    Name: 'Panama',
    Code: 'PA',
    Timezone: 'SA Pacific Standard Time',
    UTC: 'UTC-05:00',
    MobileCode: '+507'
}, {
    Name: 'Papua New Guinea',
    Code: 'PG',
    Timezone: 'West Pacific Standard Time',
    UTC: 'UTC+10:00',
    MobileCode: '+675'
}, {
    Name: 'Paraguay',
    Code: 'PY',
    Timezone: 'Paraguay Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+595'
}, {
    Name: 'Peru',
    Code: 'PE',
    Timezone: 'SA Pacific Standard Time',
    UTC: 'UTC-05:00',
    MobileCode: '+51'
}, {
    Name: 'Philippines',
    Code: 'PH',
    Timezone: 'Singapore Standard Time',
    UTC: 'UTC+08:00',
    MobileCode: '+63'
}, {
    Name: 'Pitcairn Islands',
    Code: 'PN',
    Timezone: 'Pacific Standard Time',
    UTC: 'UTC-08:00',
    MobileCode: '+870'
}, {
    Name: 'Poland',
    Code: 'PL',
    Timezone: 'Central European Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+48'
}, {
    Name: 'Portugal',
    Code: 'PT',
    Timezone: 'GMT Standard Time',
    UTC: 'UTC',
    MobileCode: '+351'
}, {
    Name: 'Puerto Rico',
    Code: 'PR',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+1-787 and 1-939'
}, {
    Name: 'Qatar',
    Code: 'QA',
    Timezone: 'Arab Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+974'
}, {
    Name: 'Reunion',
    Code: 'RE',
    Timezone: 'Mauritius Standard Time',
    UTC: 'UTC+04:00',
    MobileCode: '+262'
}, {
    Name: 'Romania',
    Code: 'RO',
    Timezone: 'GTB Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+40'
}, {
    Name: 'Russia',
    Code: 'RU',
    Timezone: 'Russian Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+7'
}, {
    Name: 'Rwanda',
    Code: 'RW',
    Timezone: 'South Africa Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+250'
}, {
    Name: 'Saint Barthélemy',
    Code: 'BL',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+590'
}, {
    Name: 'Saint Helena, Ascension and Tristan da Cunha',
    Code: 'SH',
    Timezone: 'Greenwich Standard Time',
    UTC: 'UTC',
    MobileCode: '+290'
}, {
    Name: 'Saint Kitts and Nevis',
    Code: 'KN',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+1-869'
}, {
    Name: 'Saint Lucia',
    Code: 'LC',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+1-758'
}, {
    Name: 'Saint Martin (French part)',
    Code: 'MF',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+590'
}, {
    Name: 'Saint Pierre and Miquelon',
    Code: 'PM',
    Timezone: 'Greenland Standard Time',
    UTC: 'UTC-03:00',
    MobileCode: '+508'
}, {
    Name: 'Saint Vincent and the Grenadines',
    Code: 'VC',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+1-784'
}, {
    Name: 'Samoa',
    Code: 'WS',
    Timezone: 'Samoa Standard Time',
    UTC: 'UTC+13:00',
    MobileCode: '+685'
}, {
    Name: 'San Marino',
    Code: 'SM',
    Timezone: 'W. Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+378'
}, {
    Name: 'São Tomé and Príncipe',
    Code: 'ST',
    Timezone: 'Greenwich Standard Time',
    UTC: 'UTC',
    MobileCode: '+239'
}, {
    Name: 'Saudi Arabia',
    Code: 'SA',
    Timezone: 'Arab Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+966'
}, {
    Name: 'Senegal',
    Code: 'SN',
    Timezone: 'Greenwich Standard Time',
    UTC: 'UTC',
    MobileCode: '+221'
}, {
    Name: 'Serbia',
    Code: 'RS',
    Timezone: 'Central Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+381'
}, {
    Name: 'Seychelles',
    Code: 'SC',
    Timezone: 'Mauritius Standard Time',
    UTC: 'UTC+04:00',
    MobileCode: '+248'
}, {
    Name: 'Sierra Leone',
    Code: 'SL',
    Timezone: 'Greenwich Standard Time',
    UTC: 'UTC',
    MobileCode: '+232'
}, {
    Name: 'Singapore',
    Code: 'SG',
    Timezone: 'Singapore Standard Time',
    UTC: 'UTC+08:00',
    MobileCode: '+65'
}, {
    Name: 'Sint Maarten (Dutch part)',
    Code: 'SX',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+599'
}, {
    Name: 'Slovakia',
    Code: 'SK',
    Timezone: 'Central Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+421'
}, {
    Name: 'Slovenia',
    Code: 'SI',
    Timezone: 'Central Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+386'
}, {
    Name: 'Solomon Islands',
    Code: 'SB',
    Timezone: 'Central Pacific Standard Time',
    UTC: 'UTC+11:00',
    MobileCode: '+677'
}, {
    Name: 'Somalia',
    Code: 'SO',
    Timezone: 'E. Africa Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+252'
}, {
    Name: 'South Africa',
    Code: 'ZA',
    Timezone: 'South Africa Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+27'
}, {
    Name: 'South Sudan',
    Code: 'SS',
    Timezone: 'E. Africa Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+211'
}, {
    Name: 'Spain',
    Code: 'ES',
    Timezone: 'Romance Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+34'
}, {
    Name: 'Sri Lanka',
    Code: 'LK',
    Timezone: 'Sri Lanka Standard Time',
    UTC: 'UTC+05:30',
    MobileCode: '+94'
}, {
    Name: 'Sudan',
    Code: 'SD',
    Timezone: 'E. Africa Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+249'
}, {
    Name: 'Suriname',
    Code: 'SR',
    Timezone: 'SA Eastern Standard Time',
    UTC: 'UTC-03:00',
    MobileCode: '+597'
}, {
    Name: 'Svalbard',
    Code: 'SJ',
    Timezone: 'W. Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+47'
}, {
    Name: 'Swaziland',
    Code: 'SZ',
    Timezone: 'South Africa Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+268'
}, {
    Name: 'Sweden',
    Code: 'SE',
    Timezone: 'W. Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+46'
}, {
    Name: 'Switzerland',
    Code: 'CH',
    Timezone: 'W. Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+41'
}, {
    Name: 'Syria',
    Code: 'SY',
    Timezone: 'Syria Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+963'
}, {
    Name: 'Taiwan',
    Code: 'TW',
    Timezone: 'Taipei Standard Time',
    UTC: 'UTC+08:00',
    MobileCode: '+886'
}, {
    Name: 'Tajikistan',
    Code: 'TJ',
    Timezone: 'West Asia Standard Time',
    UTC: 'UTC+05:00',
    MobileCode: '+992'
}, {
    Name: 'Tanzania',
    Code: 'TZ',
    Timezone: 'E. Africa Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+255'
}, {
    Name: 'Thailand',
    Code: 'TH',
    Timezone: 'SE Asia Standard Time',
    UTC: 'UTC+07:00',
    MobileCode: '+66'
}, {
    Name: 'Togo',
    Code: 'TG',
    Timezone: 'Greenwich Standard Time',
    UTC: 'UTC',
    MobileCode: '+228'
}, {
    Name: 'Tokelau',
    Code: 'TK',
    Timezone: 'Tonga Standard Time',
    UTC: 'UTC+13:00',
    MobileCode: '+690'
}, {
    Name: 'Tonga',
    Code: 'TO',
    Timezone: 'Tonga Standard Time',
    UTC: 'UTC+13:00',
    MobileCode: '+676'
}, {
    Name: 'Trinidad and Tobago',
    Code: 'TT',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+1-868'
}, {
    Name: 'Tunisia',
    Code: 'TN',
    Timezone: 'W. Central Africa Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+216'
}, {
    Name: 'Turkey',
    Code: 'TR',
    Timezone: 'Turkey Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+90'
}, {
    Name: 'Turkmenistan',
    Code: 'TM',
    Timezone: 'West Asia Standard Time',
    UTC: 'UTC+05:00',
    MobileCode: '+993'
}, {
    Name: 'Turks and Caicos Islands',
    Code: 'TC',
    Timezone: 'Eastern Standard Time',
    UTC: 'UTC-05:00',
    MobileCode: '+1-649'
}, {
    Name: 'Tuvalu',
    Code: 'TV',
    Timezone: 'UTC+12',
    UTC: 'UTC+12:00',
    MobileCode: '+688'
}, {
    Name: 'Uganda',
    Code: 'UG',
    Timezone: 'E. Africa Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+256'
}, {
    Name: 'Ukraine',
    Code: 'UA',
    Timezone: 'FLE Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+380'
}, {
    Name: 'United Arab Emirates',
    Code: 'AE',
    Timezone: 'Arabian Standard Time',
    UTC: 'UTC+04:00',
    MobileCode: '+971'
}, {
    Name: 'United Kingdom',
    Code: 'GB',
    Timezone: 'GMT Standard Time',
    UTC: 'UTC',
    MobileCode: '+44'
}, {
    Name: 'United States',
    Code: 'US',
    Timezone: 'Pacific Standard Time',
    UTC: 'UTC-08:00',
    MobileCode: '+1'
}, {
    Name: 'Uruguay',
    Code: 'UY',
    Timezone: 'Montevideo Standard Time',
    UTC: 'UTC-03:00',
    MobileCode: '+598'
}, {
    Name: 'Uzbekistan',
    Code: 'UZ',
    Timezone: 'West Asia Standard Time',
    UTC: 'UTC+05:00',
    MobileCode: '+998'
}, {
    Name: 'Vanuatu',
    Code: 'VU',
    Timezone: 'Central Pacific Standard Time',
    UTC: 'UTC+11:00',
    MobileCode: '+678'
}, {
    Name: 'Vatican City',
    Code: 'VA',
    Timezone: 'W. Europe Standard Time',
    UTC: 'UTC+01:00',
    MobileCode: '+379'
}, {
    Name: 'Vietnam',
    Code: 'VN',
    Timezone: 'SE Asia Standard Time',
    UTC: 'UTC+07:00',
    MobileCode: '+84'
}, {
    Name: 'Virgin Islands, U.S.',
    Code: 'VI',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+1-340'
}, {
    Name: 'Virgin Islands, British',
    Code: 'VG',
    Timezone: 'SA Western Standard Time',
    UTC: 'UTC-04:00',
    MobileCode: '+1-284'
}, {
    Name: 'Wallis and Futuna',
    Code: 'WF',
    Timezone: 'UTC+12',
    UTC: 'UTC+12:00',
    MobileCode: '+681'
}, {
    Name: 'Yemen',
    Code: 'YE',
    Timezone: 'Arab Standard Time',
    UTC: 'UTC+03:00',
    MobileCode: '+967'
}, {
    Name: 'Zambia',
    Code: 'ZM',
    Timezone: 'South Africa Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+260'
}, {
    Name: 'Zimbabwe',
    Code: 'ZW',
    Timezone: 'South Africa Standard Time',
    UTC: 'UTC+02:00',
    MobileCode: '+263'
}];

export const gc = (country: string) => {
    return countries.find((c: any) => c.name === country)
}
