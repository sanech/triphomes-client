
declare var XDomainRequest: any;
const SMTP = {
  send: (a: any) => {
    return new Promise((n, e) => {
      (a.nocache = Math.floor(1e6 * Math.random() + 1)), (a.Action = "Send");
      var t = JSON.stringify(a);
      SMTP.ajaxPost("https://smtpjs.com/v3/smtpjs.aspx?", t, (e: any) => {
        n(e);
      });
    });
  },
  ajaxPost: (e: any, n: any, t: any) => {
    let a: any = SMTP.createCORSRequest("POST", e);
    a.setRequestHeader("Content-type", "application/x-www-form-urlencoded"),
      (a.onload = function() {
        let e: any = a.responseText;
        null != t && t(e);
      }),
      a.send(n);
  },
  ajax: function(e: any, n: any) {
    var t: any = SMTP.createCORSRequest("GET", e);
    (t.onload = () => {
      var e = t.responseText;
      null != n && n(e);
    }),
      t.send();
  },
  createCORSRequest: (e: string, n: any) => {
    let t:any = new XMLHttpRequest();
    return (
      "withCredentials" in t
        ? t.open(e, n, !0)
        : "undefined" != typeof XDomainRequest
        ? (t = new XDomainRequest()).open(e, n)
        : (t = null),
      t
    );
  },
};
export default SMTP;
