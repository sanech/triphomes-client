import currency from 'currency.js'

const EUR = (summ: number) => currency(summ, { precision: 0, symbol: "€", separator: ".", decimal: "," });
const USD = (summ: number) => currency(summ, { precision: 0, symbol: "$", separator: ",", decimal: "." });
const AUD = (summ: number) => currency(summ, { precision: 0, symbol: "$", separator: ",", decimal: "." });
const CAD = (summ: number) => currency(summ, { precision: 0, symbol: "$", separator: ",", decimal: "." });
const GBP = (summ: number) => currency(summ, { precision: 0, symbol: "£", separator: ",", decimal: "." });


const money = (summ: number = 1, currency: string = 'EUR') => {
    switch (currency) {
        case 'EUR':
            return EUR(summ).format();
        case 'USD':
            return USD(summ).format();
        case 'AUD':
            return AUD(summ).format();
        case 'CAD':
            return CAD(summ).format();
        case 'GBP':
            return GBP(summ).format();
        default:
            return EUR(summ).format();
    }
}

export default money;