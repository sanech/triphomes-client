const DEFAULT_PLATFORM_ACCESS_TOKEN = "0f214a90-40d7-4db7-97b5-e90bd1993f17";
var AnypayEvents = function() {
    this.events = {}
};
AnypayEvents.prototype.on = function(e, t) {
    return "object" != typeof this.events[e] && (this.events[e] = []), this.events[e].push(t), this
}, AnypayEvents.prototype.removeListener = function(e, t) {
    var n;
    "object" == typeof this.events[e] && (n = this.events[e].indexOf(t)) > -1 && this.events[e].splice(n, 1)
}, AnypayEvents.prototype.emit = function(e) {
    var t, n, a, o = [].slice.call(arguments, 1);
    if ("object" == typeof this.events[e])
        for (a = (n = this.events[e].slice()).length, t = 0; t < a; t++) n[t].apply(this, o)
}, AnypayEvents.prototype.once = function(e, t) {
    return this.on(e, function n() {
        this.removeListener(e, n), t.apply(this, arguments)
    })
};
const anypay = (() => {
    var e = document.getElementsByTagName("HEAD")[0],
        t = document.createElement("link");
    t.href = "https://unpkg.com/@anypayinc/widget/anypay.min.css", t.rel = "stylesheet", t.type = "text/css", e.appendChild(t);
    const n = function(e, t = null, n = {}) {
            anypay.eventEmitter = t || new AnypayEvents;
            var o = document.createElement("div"),
                i = document.createElement("iframe"),
                y = document.createElement("div"),
                s = document.createElement("div");
            o.id = "anypay-iframe-wrapper", y.classList.add("anypay-fade-in"), y.id = "anypay-overlay", s.id = "anypay-loader", i.id = "anypay-iframe", i.onload = function() {
                anypay.eventEmitter.emit("loaded"), document.getElementById("anypay-loader").style.display = "none"
            };
            return i.setAttribute("src", `https://unpkg.com/@anypayinc/widget/widget/index.html#/invoices/${e}/options/${btoa(JSON.stringify(n))}`), i.setAttribute("allow", "clipboard-write"), o.appendChild(s), o.appendChild(i), y.appendChild(o), document.body.appendChild(y), y.onclick = (() => a()), document.addEventListener("keydown", function(e) {
                "Escape" != e.key && "Esc" != e.key && 27 != e.keyCode || a()
            }, !0), window.addEventListener("message", e => {
                var t = e.data;
                "anypay" == t.from && ("close" == t.event ? a() : "update" == t.event && anypay.eventEmitter.emit(t.status))
            }), anypay.eventEmitter
        },
        a = () => {
            document.getElementById("anypay-overlay").classList.replace("anypay-fade-in", "anypay-fade-out"), setTimeout(() => {
                var e = document.getElementById("anypay-overlay");
                e && (document.body.removeChild(e), anypay.eventEmitter.emit("closed"))
            }, 450)
        };
    return {
        showInvoice: n,
        collectPayment: function(e) {
            var t = new AnypayEvents;
            return fetch(`https://api.anypayinc.com/accounts/${e.accountId}/invoices`, {
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    amount: e.amount
                }),
                method: "POST",
                mode: "cors"
            }).then(e => e.json()).then(a => n(a.invoice.uid, t, {
                moneyButton: e.moneyButton,
                relayX: e.relayX,
                qrCode: e.qrCode
            })), t
        },
        newPayment: function(e) {
            var t = new AnypayEvents;
            return fetch("https://api.anypayinc.com/r", {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Basic ${btoa(DEFAULT_PLATFORM_ACCESS_TOKEN+":")}`
                },
                body: JSON.stringify(e),
                method: "POST",
                mode: "cors"
            }).then(e => e.json()).then(e => n(e.payment_request.invoice_uid, t)), t
        },
        close: a
    }
})();