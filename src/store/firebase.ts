import { FirebaseApp, initializeApp } from 'firebase/app';
import { getFirestore, collection, doc, getDoc } from 'firebase/firestore/lite';
// const firebaseConfig = {
//   apiKey: import.meta.env.VITE_APP_FB_API_KEY,
//   authDomain: import.meta.env.VITE_APP_FB_AUTH_DOMAIN,
//   databaseURL: import.meta.env.VITE_APP_FB_DATABASE_URL,
//   projectId: import.meta.env.VITE_APP_FB_PROJECT_ID,
//   storageBucket: import.meta.env.VITE_APP_FB_STORAGE_BUCKET,
//   messagingSenderId: import.meta.env.VITE_APP_FB_MSG_ID,
//   appId: import.meta.env.VITE_APP_FB_APP_ID,
//   measurementId: import.meta.env.VITE_APP_FB_MID,
// };

const firebaseConfig = {
  apiKey: "AIzaSyD4AJkrM1WHSWNRB48GbPyUyt0p-WiTSn0",
  authDomain: "tripadvisor-admin.firebaseapp.com",
  databaseURL: "https://tripadvisor-admin.firebaseio.com",
  projectId: "tripadvisor-admin",
  storageBucket: "tripadvisor-admin.appspot.com",
  messagingSenderId: "828477142626",
  appId: "1:828477142626:web:ccd6efc2ca2bc26ae6433b",
  measurementId: "G-D1PT631CLH"
};

const app: FirebaseApp = initializeApp(firebaseConfig);
const db:any = getFirestore(app);

export const homesCollection = collection(db, "homes");

export const getHomeFunction = async (homeId: string) => {
  let data;
  const docRef = doc(db, "homes", homeId);
  const docSnap = await getDoc(docRef);

  if (docSnap.exists()) {
    data = docSnap.data();
  } else {
    data = null;
  }

  return data;
}

export const getBookingFunction = async (bookinkId: string) => {
  let data;
  const docRef = doc(db, "bookings", bookinkId);
  const docSnap = await getDoc(docRef);

  if (docSnap.exists()) {
    data = docSnap.data();
  } else {
    data = null;
  }
  return data;
}




export const bookingsCollection = collection(db, "bookings");


export const visitorsCollection = collection(db, "visitors");

export const bannedRef = doc(db, "banned", "visitors");


// const bannedCollection = collection(db, "banned");

// const docRef = doc(db, "cities", "SF");


//export const bannedVisitors = await getDocs(bannedCollection);

//export const bannedVisitors = collection(db, "banned").doc('visitors').get();
