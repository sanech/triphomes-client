import { defineStore } from 'pinia'
import { useStore } from './index'

export const useModalsStore = defineStore('modals', {
  persist: false,
  state: () => ({
    sendMessage: {
      visible: false,
      loading: false,
      mailsent: false,
    },
    authenticate: {
      visible: false,
      loading: false,
      logged: false,
    }
  }),
  getters: {
    authModalVisible: (state) => state.authenticate.visible,
    authModalLoading: (state) => state.authenticate.loading,
    authModalLogged: (state) => state.authenticate.logged,
    minStay: () => {
      const mainStore = useStore();
      return mainStore.minStay;
    },
    mailSent: (state) => state.sendMessage.mailsent,
    smVisible: (state) => state.sendMessage.visible,
    smLoading: (state) => state.sendMessage.loading,
    home: () => {
      const mainStore = useStore();
      return mainStore.home;
    },
    mobile: () => {
      const mainStore = useStore();
      return mainStore.mobile;
    },
    dates: () => {
      const mainStore = useStore();
      return mainStore.getDates;
    },
    guests: () => {
      const mainStore = useStore();
      return mainStore.totalGuests;
    }
  },
  actions: {
    closeSendMessage(time: number | null = 2000) {
      const modalTimeout: NodeJS.Timeout = setTimeout(() => {
        this.sendMessage.visible = false;
        this.sendMessage.loading = false;
        clearTimeout(modalTimeout);
      }, time !== null ? time : 2000);
    },
    handleSendMessage(payload: string) {
      this.sendMessage.loading = true
      switch (payload) {
        case 'send':
          console.log('send message');
          this.sendMessage.mailsent = true;
          this.closeSendMessage();
          break;
        case 'cancel':
          console.log('cancel message');
          this.closeSendMessage(0);
          break;
        case 'open':
          console.log('Modal SM Open');
          this.sendMessage.visible = true;
          this.sendMessage.loading = false;
          break;
      }
    }
  }
});