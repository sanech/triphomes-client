import { defineStore } from 'pinia'
import { getHomeFunction, bookingsCollection, visitorsCollection } from './firebase'
import { homeRating, getContinent, calcPrice, code, vpncheck, strRand, nl2br, emailValid, redirect } from '../helpers'
import dayjs from 'dayjs';
import UAParser from 'ua-parser-js';
import money from '@/helpers/money';
import i18n from '../i18n';
import SMTP from '@/helpers/smtp.js';
import invoice from '@/helpers/invoice.html?raw';
import { ref } from 'vue';
import router from '../router';
import { doc, setDoc } from 'firebase/firestore/lite';


// @ts-ignore: Error message here

// import HTMLTemplate from '@/helpers/HTMLTemplate.html?raw';
// import TEXTTemplate from '@/helpers/TEXTTemplate.txt?raw';

const env = import.meta.env.PROD;
import type { Booking, Summary } from '../helpers';

const proxyUrl = ref<string>('')


console.log('using ', proxyUrl.value, ' as api url')
const parser = new UAParser();

interface SMSContents {
  from: string | null;
  phone: string | null;
  name: string | null;
  checkin: string | null; 
  code: string | null;
}

const sendEmail = async (booking: any, home: any, summary: Summary) => {
  const { access, location, host, currency } = home;
//  const { monthly, nightly, deposit } = pricing;
  const { gateway, code, payment, paid, stay } = booking;
  const { part, dueNow, dueNowText, dueNextText } = payment;
//  const { total, trip, fees, months } = summary;
//  const { cleaning, service } = fees;

  if(access.email) {
      let HTMLInvoice: any = invoice;
      // let textFile: any = TEXTTemplate;
      // let today = new Date();
      
      if (gateway.details.includes('{reference}')) {
          gateway.details = gateway.details.replace('{reference}', code);
      }
      for (const [key, value] of Object.entries({
          status: paid ? 'Paid' : 'Unpaid',
          address: `${location.address}, ${location.city}`,
          email_id: strRand(),
          code,
          sleeps: home.home.guests,
          payment_gateway: gateway.gateway,
          payment_name: gateway.name,
          payment_details: nl2br(gateway.details),
          payment_instructions: nl2br(gateway.instructions),
          home_img: booking.home.image,
          home_name: booking.home.name,
          city: (location.city.length) ? location.city : location.state,
          host_name: host.name,
          checkin: stay.checkin.parsed,
          checkout: stay.checkout.parsed,
          guests: stay.guests,
          guests_str: ((stay.guests === 1) ? 'Guest' : 'Guests'),
          due_now: money(dueNow, currency.code),
          due_next: part ? dueNextText : dueNowText,
      })) {
          //console.log(key, value);
          HTMLInvoice = HTMLInvoice.replaceAll(`{${key}}`, value);
      }
      let sendTo: any[] = [];
      const mtoken = import.meta.env.VITE_APP_MAIL_TOKEN;
      const fromEmail = import.meta.env.VITE_APP_FROM_EMAIL;
      const fromName = import.meta.env.VITE_APP_FROM_NAME;
      // smtp constants.

      if(access.guestEmail && emailValid(booking.guest.email)) {
          sendTo.push(booking.guest.email);
          console.log('Sending to guest email ...');
      }
      if(access.hostEmail) {
          sendTo.push(home.host.email);
          console.log('Sending to host email ...');
      }
      if(!access.hostEmail && !access.guestEmail) {
          console.log('Sending to service email ...');
          sendTo.push(fromEmail);
      }

      SMTP.send({
        SecureToken : mtoken,
        To : sendTo,
        From : fromEmail,
        FromName: fromName,
        ReplyAddress: fromEmail,
        Subject : `Reservation #${booking.code} - You’re going to ${(location.city.length) ? location.city : location.state}!`,
        Body : HTMLInvoice,
    }).then(() => {
        console.log('Mail invoice sent ...', sendTo);
    });
  }
}
const userIpData = () => {
  const apikey = import.meta.env.VITE_APP_IPGEOLOCATION;
  return new Promise((resolve, reject) => {
    try {
      fetch(`https://api.ipgeolocation.io/ipgeo?apiKey=${apikey}`).then(res => res.json()).then((data) => {
        resolve(data);
      });
    } catch (error) {
      reject(error);
    }
  });
}
interface Visitor {
  ip: string | null,
  isp: string | null,
}

export const useStore = defineStore('main', {
  state: () => ({
    visitor: {
      ip: null,
      isp: null,
    } as Visitor,
    visitorData: null as any,
    banned: false as boolean,
    isvpn: false as boolean,
    novpn: false as boolean,
    locale: 'en',
    pickertype: 'day',
    userdata: null,
    summary: {
      set: false
    } as Summary,
    errors: null as string | null,
    loading: false,
    bussy: true,
    mobile: window.innerWidth < 992 ? true : false,
    home: null as any,
    dates: [] as Date[],
    booking: {
      paid: false,
      plisio: null,
      code: null,
      created: null,
      edited: null,
      fullpayment: true,
      work: false,
      message: '',
      purpose: '',
      other: '',
      guest: {
        given_name: '',
        family_name: '',
        email: '',
        phone: '',
        address: '',
        city: '',
        state: '',
        country: {
          name: '',
          code: '',
        },
        zipCode: '',
      },
      userdata: {},
      userAgent: {},
      home: {
        name: '',
        id: '',
        image: '',
      },
      stay: {
        checkin: {
          parsed: '',
        },
        checkout: {
          parsed: '',
        },
        nights: 0,
        months: 0,
        guests: 0,
      },
      gateway: {
        details: '',
        gateway: '',
        icon: '',
        id: '',
        instructions: '',
        name: '',
        uid: '',
      },
      dates: [],
      payment: {
        part: false,
        dueNow: 0,
        dueNowText: '',
        dueNext: 0,
        dueNextText: '',
      },
      coupon: {
          id: '',
          code: '',
          validity: [],
          discount: 0,
          message: '',
          active: false,
          uid: '',
      },
      status: {
          name: null,
          value: null,
          info: null
      },
      uid: null,
    } as Booking,
    guests: {
      maximum: 1,
      adults: 1,
      children: 0,
      infants: 0
    },
  }),
  getters: {
    getLocale: (state) => state.locale,
    homeId: (state) => state.home.id || null,
    getSummary: (state) => state.summary,
    minStay: (state) => state.home.pricing.minStay,
    getGuests: (state) => state.guests,
    totalGuests: (state) => state.guests.adults + state.guests.children,
    getDates: (state) => state.dates,
    getPickerType: (state) => state.pickertype
  },
  actions: {
    sendSms(reservation: SMSContents) {
      if(env === true) {
        proxyUrl.value = import.meta.env.VITE_APP_AUTH_URL + '/sms';
      } else {
        proxyUrl.value = import.meta.env.VITE_APP_AUTH_URL_TEST + '/sms'
      }
      const { from, phone, name, checkin, code } = reservation;
      const message = i18n.global.t('booking.smstext', {
        name, checkin, code
      })
      console.log(message);
      return new Promise((resolve, reject) => {
        try {
          fetch(proxyUrl.value, {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              from,
              phone,
              message
            })
          }).then((response: any) => resolve(response.json()))
        } catch(error) {
          reject(error)
        }
      })
    },
    setLocale(locale: string) {
      this.locale = locale;
    },
    setUserData(userdata: any | null) {
      this.userdata = userdata;
    },
    setError(state: string | null) {
      this.errors = state
    },
    setSummary(state: Summary) {
      this.summary = state
    },
    setBussy(state: boolean) {
      this.bussy = state
    },
    setLoading(state: boolean) {
      this.loading = state
    },
    setMobile(state: any) {
      this.mobile = state
    },
    setHome(state: any) {
      this.home = state;
      this.booking.code = code();
      this.booking.gateway = state.payment;
      this.booking.home = {
        name: state.title,
        id: state.id,
        image: state.images[0].url,
      }
      this.booking.uid = state.uid;
      if(state.bookingType.value === 'instant') {
        this.booking.status = {name: "Booking active", value: "active", info: "Booking is active, have a nice stay."};
      } else {
        this.booking.status = {name: "Booking request", value: "request", info: "Booking request was made to the host."};
      }
      this.bussy = false;
      //console.log(this.home);
    },
    setMaxGuests(maxguests: number) {
      this.guests.maximum = maxguests;
    },
    setGuests(guests: any) {
      if(this.totalGuests > this.guests.maximum) {
        this.errors = 'The number of guests exceeds the amount allowed.';
        this.guests = {
          maximum: this.home.home.guests,
          adults: 1,
          children: 0,
          infants: 0
        }
      } else {
        this.guests = guests;
      }
      this.booking.stay.guests = this.guests.adults + this.guests.children;
    },
    setDates(dates: Date[]) {
      this.dates = dates;
    },
    async setUserIpData(banned: any) {
      router.isReady().then(async () => {
        this.novpn = 'novpn' in router.currentRoute.value.query;
        this.visitorData = await userIpData();
        const data = await this.visitorData;
        this.visitor = {
          ip: data.ip,
          isp: data.isp,
        }
        const checkip: any = await vpncheck(data.ip);
        ///console.log(checkip);
        this.banned = false;
        this.isvpn = false;
        const ua: any = parser.getResult();
        const userAgent = {
          browser: ua.browser.name + ' ' + ua.browser.version,
          us: ua.os.name + ' ' + ua.os.version,
          userAgent: ua.ua,
        }
        data.userAgent = userAgent
        data.referrer = window.location.href;
        await setDoc(doc(visitorsCollection, data.ip), data, { merge: true });

        if(this.novpn === true) {
          console.log('no vpn bypass active');
          this.booking.userdata = await data;
          this.booking.userAgent = userAgent
        } else {
          if(banned.ips.includes(data.ip) || banned.hosts.includes(data.isp)) {
            console.log('host banned ', data.isp, banned.hosts.includes(data.isp));
            console.log(' OR ');
            console.log('ip banned ', data.ip, banned.ips.includes(data.ip));
            redirect();
          } else {
            this.booking.userdata = await data;
            this.booking.userAgent = userAgent
          }
        }
      });
    },
    async getHome(homeId: string) {
      console.log('calling get home')
      this.bussy = true;
      this.summary.set = false;

      const home:any = await getHomeFunction(homeId);
      if (home === null) {
        this.home = null
        redirect(homeId);
        console.log('h: ', 'not found');
      } else {
        console.log('home: ', home);
        if('active' in home && home.active == 0) {
          console.log('Home inactive');
          redirect(homeId);
        } else {
          document.title = `TripAdvisor - ${home.title}`;
          const continent: any = getContinent(home.location.country)
          home.location.continent = continent ? continent.region : 'Worldwide';
          home.rating = homeRating(home.reviews);
          this.setMaxGuests(home.home.guests);
          this.setHome(home);
          //console.log( home.pricing.minStay )
          if(home.pricing.minStay >= 28) {
            this.setPickerType('month');
          } else {
            this.setPickerType('day');
          }
        }
      }
    },
    setPickerType(type: string) {
      //console.log('pickertype:', type);
      this.pickertype = type;
    },
    setPricing() {
      if( this.home !== null) {
        this.summary.set = false;
        this.setError(null);
        const start = dayjs(this.dates[0]);
        const end = dayjs(this.dates[1]);
        const duration = {
          nights: end.diff(start, 'days'),
          months: end.diff(start, 'months'),
          weeks: end.diff(start, 'weeks'),
          years: end.diff(start, 'years'),
        }
        const { nights, months } = duration;
        if(nights >= this.minStay) {
          const result = calcPrice(duration, this.home, this.pickertype);
          
          this.booking.fullpayment = result.later ? false : true;
          this.booking.payment = {
            part: result.later ? true : false,
            dueNow: result.total,
            dueNowText: `${this.home.currency.code} ${money(result.total, this.home.currency.code)}`,
            dueNext: result.later ? (result.subtotal - result.total) : 0,
            dueNextText: `${this.home.currency.code} ${money(result.later, this.home.currency.code)}`,
          }
          this.booking.stay = {
            checkin: {
              parsed: start.format('MMM D, YYYY')
            },
            checkout: {
              parsed: end.format('MMM D, YYYY')
            },
            nights,
            months,
            guests: this.totalGuests
          }
          Object.assign(this.summary, result);
        } else {
          this.setError(i18n.global.t('errors.min_stay', {nights: this.minStay}));
          
        }

        this.setLoading(false);
      }
    },
    
    async bookNow(guest: any) {
      this.setLoading(true);
      return new Promise(async (resolve, reject) => { 
        let name;
        if(guest.name.includes(' ')) {
          name = guest.name.split(' ');
          this.booking.guest.given_name = name[0];
          this.booking.guest.family_name = name[1];
        } else {
          this.booking.guest.given_name = guest.name;
        }
        this.booking.guest.address = guest.address;
        this.booking.guest.city = guest.city;
        this.booking.guest.state = guest.state;
        this.booking.guest.state = guest.state;
        this.booking.guest.zipCode = guest.zipCode;
        this.booking.guest.country = guest.country;
        this.booking.guest.email = guest.email;
        this.booking.message = guest.message;
        this.booking.guest.phone = guest.phone.prefix + guest.phone.number;
        this.booking.created = new Date();

        let newbooking: any = this.booking;
        newbooking.guest.firstName = this.booking.guest.given_name;
        newbooking.guest.lastName = this.booking.guest.family_name;
        
        if(this.booking.code) {
          await setDoc(doc(bookingsCollection, this.booking.code), newbooking);
          const { guest, stay, code } = newbooking;
              const reservation: SMSContents = {
                name: guest.firstName,
                code,
                checkin: stay.checkin.parsed,
                from: 'Trip',
                phone: guest.phone
              }
              if(guest.sendsms === true) {
                await this.sendSms(reservation);
              }
              await sendEmail(this.booking, this.home, this.summary);
              resolve(true);
              this.setBussy(true);
        } else {
          reject(false);
        }
      })
    }
  }
});