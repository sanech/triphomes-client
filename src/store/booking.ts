import { defineStore } from 'pinia'
import { homesCollection, bookingsCollection, getBookingFunction, getHomeFunction } from './firebase'
import type { Booking, Summary } from '../helpers'
import { calcPrice } from '../helpers';
import dayjs from 'dayjs';
import { useStore } from './index'
import { doc, setDoc } from 'firebase/firestore/lite';
const useBooking = defineStore('booking', {
  state: () => ({
    valid: null as boolean | null,
    userdata: null,
    summary: {
      set: false
    } as Summary,
    errors: null as string | null,
    loading: false,
    bussy: true,
    mobile: window.innerWidth < 992 ? true : false,
    home: null as null | any,
    dates: [] as Date[],
    booking: null as Booking | any,
  }),
  getters: {
    getPaid: (state) => state.booking.paid || false,
    getMobile: (state) => state.mobile,
    getLoading: (state) => state.loading,
    getBussy: (state) => state.bussy,
    getBooking: (state) => state.booking || null,
    getHome: (state) => state.home || null,
    homeId: (state) => state.home.id || null,
  },
  actions: {
    setBussy(state: boolean) {
      this.bussy = state
    },
    setLoading(state: boolean) {
      this.loading = state
    },
    setValid(state: boolean | null) {
      this.valid = state
    },
    async setBookingPaid(paid: boolean) {
      return new Promise(async(resolve, reject) => {
        if(this.booking.code) {
          await setDoc(doc(bookingsCollection, this.booking.code), {paid: paid}, { merge: true });
          resolve(true);
        } else {
          reject(false);
        }
      });
    },
    async updateBooking(plisio: any) {
      return new Promise(async (resolve, reject) => {
        if(this.booking.code) {
          await setDoc(doc(bookingsCollection, this.booking.code), {plisio: plisio}, { merge: true });
          resolve(true);
        } else {
          reject(false);
        }
      });
    },
    async fetchBooking(code: any) {
      this.summary.set = false;
      this.setLoading(true);
      this.setBussy(true);
      return new Promise(async (resolve, reject) => {
        const booking = await getBookingFunction(code);
        if(booking !== null) {
          this.booking = booking;
          if('paid' in this.booking) {
            //console.log('paid object ', this.booking.paid);
          } else {
            this.booking.paid = false;
          }

          const homeId = this.booking.home.id.toString();

          const home = await getHomeFunction(homeId);
          if (home !== null) {
            const mainStore = useStore();
            const { pickertype } = mainStore;
            this.setValid(true);
            const start = dayjs(this.booking.stay.checkin.parsed);
            const end = dayjs(this.booking.stay.checkout.parsed);
            this.home = home;

            const duration = {
              nights: end.diff(start, 'days'),
              months: end.diff(start, 'months'),
              weeks: end.diff(start, 'weeks'),
              years: end.diff(start, 'years'),
            }
            const result = calcPrice(duration, this.home, pickertype);
            Object.assign(this.summary, result);
            resolve(true);
          }
        } else {
          this.setValid(false);
          reject(false);
        }
      });
    }
  }
});

export default useBooking;