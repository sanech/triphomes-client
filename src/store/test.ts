import { defineStore } from 'pinia';

const useTest = defineStore('test', {
    persist: true,
    state: () => ({
        ids: [] as number[],
    }),
    actions: {
        addId(payload: number) {
            // push if not already in array
            if (!this.ids.includes(payload)) {
                this.ids.push(payload);
            }
        },
        removeId(payload: number) {
            this.ids.splice(this.ids.indexOf(payload), 1);
        }
    }
});

export default useTest;