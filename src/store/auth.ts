import { defineStore } from 'pinia';
interface User {
    id: number;
    given_name: string;
    family_name: string;
    name: string;
    email: string;
    password: string;
    picture: string;
    provider: string;
    provider_id: number;
    created_at: Date | string;
    updated_at: Date | string;
}

const useAuth = defineStore('auth', {
    persist: true,
    state: () => ({
        authmodalstate: false as boolean,
        saved: false,
        logged: false,
        user: {
            id: 0,
            given_name: '',
            family_name: '',
            name: '',
            email: '',
            password: '',
            picture: '',
            provider: '',
            provider_id: 0,
            created_at: '',
            updated_at: '',
        } as User,
        userdata: null,
        errors: null,
        loading: false,
        modal: false,
    }),
    getters: {
        getAuthModalState: (state) => state.authmodalstate,
    },
    actions: {
        setAuthModalState( state: boolean ) {
            this.authmodalstate = state;
          },
        setSaved(payload: boolean) {
            this.saved = payload;
        },
        login(payload: any) {
            this.loading = payload;
        },
        setLoading(payload: any) {
            this.loading = payload;
        },
        logout(refresh = false) {
            this.logged = false;
            this.user = {
                id: 0,
                given_name: '',
                family_name: '',
                name: '',
                email: '',
                password: '',
                picture: '',
                provider: '',
                provider_id: 0,
                created_at: '',
                updated_at: '',
            };
            this.userdata = null;
            this.errors = null;
            this.modal = false;
            if(refresh === true) {
                window.location.reload();
            }
            // refresh page
            
        },
        setUser(payload: any) {
            this.user = payload;
            this.setLoading(false);
            this.setLogged(true);
        },
        setUserData(payload: any) {
            if(payload !== null) {
                let userfrombase64: string | any = window.atob(payload);
                userfrombase64 = JSON.parse(userfrombase64);
                this.userdata = payload;
                this.setUser(userfrombase64);
            }
        },
        setLogged(payload: boolean) {
            this.logged = payload;
        },
        setModal(payload: boolean) {
            this.modal = payload;
        }

    }
});

export default useAuth;